# website

A documentation and home page used as website for [Gadgetbridge](https://codeberg.org/Freeyourgadget/Gadgetbridge), which is built with [Material for Mkdocs](https://squidfunk.github.io/mkdocs-material/).

> Content comes from the current wiki. Most of the content has been re-organized and improved formatting, but it is not finalized yet. Check [this discussion](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/3285) for more details.

## Building

Mkdocs is written in Python, so as a bare minimum, Python environment is required to get running the documentation. Other than required Python dependencies, this documentation doesn't require additional mandatory setup.

Having a virtual Python environment is **strongly recommended** (and required for scripts) to not break user/system-wide Python installations. One of ways to have per-project Python installations is installing [`rye`](https://rye-up.com/).

Commands are available in `./tasks.sh` bash script to make working with the documentation easier. Here are some commands;

Install dependencies:

```
$ ./tasks.sh deps deps_tools
```

`deps` will require having `rye` installed, because since `rye` doesn't expose `pip` it has own command to install and manage dependencies. If you use another Python version/package manager, calling `pip install .` (or setuptools-compatible tool) should be enough as the `pyproject.toml` file doesn't use non-standard keys.

`deps_tools` downloads few command line tools to current folder, which later used for minifying the generated documentation. (HTML & images) It is not required to have these tools to have documentation built, so you can omit this command (by simply running `./tasks.sh deps` alone) if you don't want to download them.

Preview documentation locally:

```
$ ./tasks.sh serve
```

Build documentation:

```
$ ./tasks.sh dist
```

Publish documentation:

```
$ ./tasks.sh publish
```

By default, it pushes the documentation to `pages` branch in [`Freeyourgadget/gadgetbridge_org`](https://codeberg.org/Freeyourgadget/gadgetbridge_org) repository. 

To publish the documentation, it needs to be first built with `./tasks.sh dist`. The script will fail if there is no output folder created by `dist` task. To do the both, (build & deploy), simply run `./tasks.sh dist publish`.

## License

All content in this repository has licensed with [AGPLv3](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/LICENSE) unless written otherwise.
