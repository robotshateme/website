#!/usr/bin/env python3

#
#    Copyright (C) 2023 ysfchn
#
#    This file is part of Gadgetbridge.
#
#    Gadgetbridge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Gadgetbridge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
    Clones the wiki git repository in a temporary folder and saves
    a "report.yml" containing the latest information of the wiki.
"""

from tempfile import TemporaryDirectory
from dulwich.client import HttpGitClient
from dulwich.repo import Repo
from dulwich.objects import Commit
from pathlib import Path
from datetime import datetime
import yaml

def clone_wiki(target : Path) -> Repo:
    client = HttpGitClient("https://codeberg.org")
    return client.clone(
        "Freeyourgadget/Gadgetbridge.wiki.git", 
        target, mkdir = False
    )

def generate_report(repo : Repo):
    pages = []
    assets = {}
    index = repo.open_index()
    for i in Path(repo.path).glob("**/*.*"):
        file_path = i.relative_to(repo.path).as_posix()
        if file_path.startswith(".git"):
            continue
        git_sha = index[file_path.encode()].sha.decode()
        if i.suffix != ".md":
            assets[file_path] = {
                "sha": git_sha
            }
        else:
            text = i.read_bytes()
            pages.append({
                "id": file_path,
                "lines": len(text.split(b"\n")),
                "sha": git_sha,
                "raw_url": "https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/raw/{0}" \
                    .format(i.stem),  # noqa: E501
                "url": "https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/{0}" \
                    .format(i.stem)
            })
    done = sorted(pages, key = lambda x: x["lines"], reverse = True)
    commit : Commit = repo[repo.head()]
    commit_sha = commit.sha().hexdigest()
    return yaml.dump({
        "meta": {
            "date": datetime.fromtimestamp(commit.commit_time).isoformat(),
            "sha": commit_sha,
            "author": commit.author.decode(),
            "message": commit.message.decode().strip(),
            "url": \
                "https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/commit/{0}" \
                .format(commit_sha)
        },
        "pages": done,
        "assets": assets
    }, default_flow_style = False, sort_keys = False)


def main():
    temp_dir = TemporaryDirectory()
    temp_dir_path = Path(temp_dir.name).resolve()
    repo = clone_wiki(temp_dir_path)
    report = generate_report(repo)
    with Path("./report.yml").open("w+", encoding = "utf-8") as f:
        f.write(report)
    temp_dir.cleanup()

if __name__ == "__main__":
    main()