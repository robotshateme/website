#!/usr/bin/env python3

#
#    Copyright (C) 2023 ysfchn
#
#    This file is part of Gadgetbridge.
#
#    Gadgetbridge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Gadgetbridge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
    A script that fetches all blog posts from blog.freeyourgadget.org RSS feed,
    and tries to convert the HTML content to Markdown by converting tags manually.

    It is used for migrating blog pages to Markdown. The converted markdown
    is not perfect, so manual editing may be needed. We probably could use a library
    instead to convert HTML > Markdown directly, but I couldn't find a 
    good/well-supported library that does this job.
"""

import httpx
from bs4 import BeautifulSoup, Tag
from typing import Dict
from pathlib import Path

FEED_URL = "https://blog.freeyourgadget.org/feeds/all.atom.xml"
SUMMARY_CHAR = 500

# Name of the authors as appears in the blog.freeyourgadget.org,
# mapped to an key, which is later used in .authors.yml. 
AUTHOR_ID = {
    "Andreas Shimokawa": "ashimokawa",
    "ChatGPT": "chatgpt",
    "Petr Vaněk": "vanous",
    "Daniele Gobbetti": "danielegobbetti",
    "Gadgetbridge developers": "gadgetbridge"
}

def fetch_posts(feed_url : str) -> Tag:
    req = httpx.get(feed_url, trust_env = False)
    req.raise_for_status()
    soup = BeautifulSoup(req.text, "xml")
    for entry in soup.find("feed").find_all("entry"):
        yield entry

def convert_post(post : Tag) -> Dict[str, str]:
    return {
        "slug": post.find("id").text.split(":")[-1][1:-5],
        "original": post.find("link")["href"],
        "title": post.find("title").text,
        "date": post.find("published").text,
        "author": post.find("author").find("name").text,
        "content": html_to_md(
            BeautifulSoup(post.find("content").text, "lxml")
        )
    }

def create_post(post : Dict[str, str], authors : Dict[str, str]) -> str:
    return "\n".join([
        "---", "slug: {slug}",
        "date: {date}", "authors:",
        "  - {author}", "categories:",
        "  - {category}", "---", "",
        "# {title}", "", "{content}", "",
        "<!-- Converted from: {original} -->"
    ]).format(
        date = post["date"],
        author = authors.get(post["author"], "unknown"),
        title = post["title"],
        content = post["content"],
        original = post["original"],
        slug = post["slug"],
        category = \
            "Releases" if post["slug"].startswith("release-") else \
            "Uncategorized"
    )

def html_to_md(soup : BeautifulSoup) -> str:
    out = ""
    for item in soup.find("body").find_all(recursive = True):
        if item.name == "p":
            out += item.text + "\n\n"
        elif item.name in ["h4", "h3", "h2", "h1"]:
            if out.endswith("\n\n"):
                pass
            elif out.endswith("\n"):
                out += "\n"
            else:
                out += "\n\n"
            out += (
                "## " if item.name in ["h1", "h2"] else
                "### " if item.name == "h3" else
                "#### "
            ) + item.text + "\n\n"
        elif item.name == "ul":
            pass
        elif item.name == "li":
            out += "* " + item.text + "\n"
        elif item.name == "hr":
            out += "---\n\n"
        elif item.name == "a":
            out += '[{0}]({1}){{: target="_blank" }}'.format(
                item.text, item["href"]
            )
        elif item.name == "em":
            out += "*" + item.text + "*"
        elif item.name == "strike":
            out += "~~" + item.text + "~~"
        elif item.name in ["strong", "b"]:
            out += "**" + item.text + "**"
        elif item.name == "img":
            out += \
                "<!-- TODO: image needed from https://blog.freeyourgadget.org" + \
                item["src"] + " -->\n\n"
        elif item.name == "code":
            out += "`" + item.text + "`"
        elif item.name == "blockquote":
            out += "> " + item.text + "\n\n"
        elif item.name == "table":
            columns = []
            table = ["", ""]
            for x in item.find("thead").find_all("th"):
                columns.append(x.text)
            for col in columns:
                table[0] += " | " + col
                table[1] += "|" + ((len(col) + 2) * "-")
            table[0] = table[0][1:] + " |"
            table[1] = table[1] + "|"
            for x in item.find("tbody").find_all("tr"):
                row = " | ".join([
                    x.text for x in x.find_all("td")
                ])
                table.append("| " + row + " |")
            out += "\n\n" + "\n".join(table) + "\n\n"
        elif item.name in ["thead", "th", "tr", "td", "tbody"]:
            pass
        elif item.name in [
            "figure", "figcaption", "div", "span", "pre", 
            "summary", "script", "noscript", "br"
        ]:
            pass
        elif item.name == "details":
            out += "<!-- " + str(item) + " -->\n\n"
        else:
            raise ValueError(item)
    # Summary text.
    count = 0
    for blob in str(out).split("\n"):
        count += len(blob)
        if count > SUMMARY_CHAR:
            out = out.replace(blob.strip(), "... <!-- more -->\n\n" + blob, 1)
            break
    return out

def main():
    posts_dir = Path(".") / "converted-blog-posts"
    posts_dir.mkdir(exist_ok = True)
    for post in fetch_posts(FEED_URL):
        converted = convert_post(post)
        created = create_post(converted, AUTHOR_ID)
        post_file : Path = posts_dir / (converted["slug"].replace("_", "-") + ".md")
        with post_file.open("w+", encoding = "utf-8") as file:
            file.write(created)

if __name__ == "__main__":
    main()