---
title: How to create a new release
---

# How to create a new release

0. Make sure you are on master and on current HEAD
1. Fetch and rebase Weblate repo
2. Add/commit translations, if applicable
3. Update changelogs:
    * `CHANGELOG.md` 
    * `app/src/main/res/xml/changelog_master.xml`
    * `fastlane/metadata/android/en-US/changelogs/XXXX.txt` (XXXX is a versionCode)
4. Adjust **versionName** and **versionCode** in app/build.gradle
5. Unarchive Github, push to it
6. Make sure it builds and testcases succeed (ideally)
7. Archive Github
8. Push the changes
9. Tag the version and push the tag:
    * `git tag versionCode`
    * `git push upstream/versionCode`
    * Note: _remember to (re)move your local Tag and never do git push --tags_
10. Party!
11. Write a blog post and publish it
12. Write a short Mastodon post
