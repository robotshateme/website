---
title: Weather app support
---

# Weather app support

Gadgetbridge weather support works as follows:

* External weather apps send an intent with the weather data, which gets forwarded to the connected device.
* Gadgetbridge caches that weather data, for devices that request it themselves (eg. Zepp OS).

There are 5 receivers for weather data:

* WeatherNotificationReceiver (for the Weather Notification app)
* TinyWeatherForecastGermanyReceiver (for the Tiny Weather Forecast Germany app)
* CMWeatherReceiver (for CyanogenMod)
* LineageOsWeatherReceiver (for LineageOS)
* GenericWeatherReceiver (originally for QuickWeather, but can be used by any app)

While apps can use any of the receivers, as long as they conform to the format they expect, new apps integrating with Gadgetbridge should use the `GenericWeatherReceiver`, which was added in {{ 2830|pull }}. This receiver expects an intent with a JSON object matching the [WeatherSpec](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/model/WeatherSpec.java){: target="_blank" } class fields.

* Action: `nodomain.freeyourgadget.gadgetbridge.ACTION_GENERIC_WEATHER`
* Field: `WeatherJson`

For example:

```bash
adb shell am broadcast -a "nodomain.freeyourgadget.gadgetbridge.ACTION_GENERIC_WEATHER" --es WeatherJson "{'timestamp':1664117628,'location':'Atlanta','currentTemp':291,'todayMinTemp':291,'todayMaxTemp':301,'currentCondition':'broken clouds, Rain in 25 mins','currentConditionCode':803,'currentHumidity':79,'windSpeed':5.75,'windDirection':290,'forecasts':[{'conditionCode':501,'humidity':68,'maxTemp':301,'minTemp':291},{'conditionCode':800,'humidity':25,'maxTemp':300,'minTemp':290},{'conditionCode':800,'humidity':15,'maxTemp':298,'minTemp':288},{'conditionCode':804,'humidity':21,'maxTemp':295,'minTemp':287},{'conditionCode':804,'humidity':36,'maxTemp':292,'minTemp':287},{'conditionCode':502,'humidity':93,'maxTemp':286,'minTemp':284},{'conditionCode':501,'humidity':96,'maxTemp':288,'minTemp':285},{'conditionCode':803,'humidity':54,'maxTemp':297,'minTemp':286}]}" nodomain.freeyourgadget.gadgetbridge
```

The QuickWeather implementation for this [can be found here](https://github.com/TylerWilliamson/QuickWeather/blob/537dc08085cac91e68969a2b98fc5f4492098bcd/app/src/main/java/com/ominous/quickweather/api/Gadgetbridge.java){: target="_blank" }.

It's possible to validate / debug the weather data that Gadgetbridge received, by clicking the "Show Cached Weather" button in the debug menu (added in {{ 3077|pull }}). This button only works for receivers that are present in the manifest, such as the `GenericWeatherReceiver`. Other receivers are only started if a device that supports weather is connected, and will not work otherwise.
