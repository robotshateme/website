---
title: Amazfit Cor 2 Firmware Update
---

# Amazfit Cor 2 Firmware Update

--8<-- "blocks.md:firmware_danger"

## Getting the firmware

Since we may not distribute the firmware, you have to do a little work. You need to find and download a Mi Fit APK file. There is an APK Mirror Web site that might help you find Mi Fit. Extract the downloaded .apk file with "unzip", and you will find an `assets/` directory containing `*.fw` files, among others.

The Amazfit Cor 2 requires the  `Mili_beats.*` files. It seems that devices with the Bluetooth name "Amazfit Band 2" are meant to user the `Mili_beats_w.*` files.

## Installing the firmware

Copy the desired Amazfit Cor 2 firmware and resource files as a `*.res` and `*.fw` file to your Android device and open it from any file manager on that device. The Gadgetbridge FW/App Installer activity should then be started and guide you through the installation process.

### Recommended flashing order

1. `Mili_beats.fw`
2. `Mili_beats.res` (the gadget will tell you when needed)

Flashing the `*.fw` triggers a reboot and will show a screen which will ask you to flash the .res if your version is outdated.

**Note 1:** Both upgrade and downgrade of firmware versions is possible.
