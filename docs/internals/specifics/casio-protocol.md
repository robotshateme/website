---
title: Casio Protocol
---

# Casio Protocol

The implementation for Gadgetbridge is based on [Bluewatcher](https://github.com/masterjc/bluewatcher){: target="_blank" } and additional analysis work. There is no formal specification of the protocol available.
