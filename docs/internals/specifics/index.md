---
title: Specifics
icon: material/database
---

# Specifics

This category contains details about specific subjects.
