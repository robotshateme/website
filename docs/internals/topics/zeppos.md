---
title: Zepp OS extras
---

# Zepp OS extras

## Experimental settings

Use [Device Settings Intent API](../automations/intents.md#device-settings-intent-api) with `zepp_os_experimental_features` key to enable experimental settings for Zepp OS.

!!! warning "Warning"
    Experimental features may be unstable. Proceed at your own risk!

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.action.SET_DEVICE_SETTING" \
    -e "device" "xx:xx:xx:xx:xx" \
    -e "key" "zepp_os_experimental_features" \
    -e "value" "true"
```

## Decoding workout summary

The workout summary consists of 2 bytes containing the summary version, followed by a protobuf encoded payload.

You can decode it using the following, for version 0x0080:

```bash
cat null_summary.bin | \
    dd bs=1 skip=2 | \
    protoc --decode WorkoutSummary --proto_path ~/workspace/Gadgetbridge/app/src/main/proto huami.proto
```

## Change region

You can use the [Device Settings Intent API](../automations/intents.md#device-settings-intent-api) to change the device region, by setting the `device_region` string preference (added in {{ "483ef27a4f"|commit }}). The region should be a 2-letter code (eg. `de` for Germany).
