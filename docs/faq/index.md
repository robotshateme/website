---
title: Frequently asked questions
icon: material/comment-question
---

# Frequently asked questions

## Is Gadgetbridge a firmware for smart watches? {: #is-it-a-firmware }

No, Gadgetbridge is an Android companion app for smart devices like bands, watches, ear buds, multimeters and other gadgets.

## Why does Gadgetbridge ask for so many permissions? {: #why-these-permissions }

In order to provide data "on the wrist", Gadgetbridge must get the data first. See [List of requested permissions](../basics/topics/permissions.md) for more details. To ensure that none of the accessed data is ever forwarded anywhere, Gadgetbridge **does not have (and will not have)** Android network permission, which means that Gadgetbridge has no means of sending any data anywhere. This is one of the most critical safeguards in the Gadgetbridge permission system.

## Can we have a Weather app implemented directly into Gadgetbridge? {: #weather-app }

No, as this would require network permission, see [here](#why-these-permissions).

## What is the best device for Gadgetbridge? {: #best-device }

See the dedicated [What is the best device?](./best-device.md) section.

## Do you support watches with Asteroid OS? {: #asteroid-os }

Yes! Initial support is released, with more development work ongoing. Please feel free to help improve it.

## When will a new release appear on F-Droid? {: #when-new-release }

Unfortunately we have no control over when a new release will appear on F-Droid, but there are a few steps you can perform to check by yourself and follow the progress. See also [When will a new release appear on F-Droid?](./when-release.md)

## Is there a way to get more frequent updates of Gadgetbridge? Like a Nightly release? {: #is-there-nightly }

Yes, you can use [our Nightly F-Droid repository.](index.md#nightly-releases) You need to install F-Droid, add our repository, update F-Droid and install Gadgetbridge Nightly.

This release is automatically built and released every night when new commits have been added to our repository. The Nightly release comes in two build types - "normal" Nightly release and "No Pebble provider" Nightly release. The "No Pebble provider" release was created due to the fact that if you have either the [official Gadgetbridge F-Droid release]({{ constants.fdroid }}){: target="_blank" } or the original Pebble app installed, you cannot install another app (for example the Gadgetbridge Nightly release) which provides something called "Pebble provider" (com.getpebble.android.provider). The "No Pebble provider" version allows to be installed alongside existing an Gadgetbridge or Pebble app. If you are planning to use the Nightly release and want to migrate your data from the official release, make sure to be careful when exporting and backing up your data. See detailed explanation in [Data Management](../internals/development/data-management.md).

## I am considering to switch to the Gadgetbridge (Nightly) release. Is it stable enough to use it instead of the stable release? {: #is-nightly-stable }

The Nightly build is a nightly build of the master branch. The master branch is what most developers of Gadgetbridge use, except that they have to self-compile it more often, while the Nightly is prepared automatically. So yes, it is and has been pretty stable. Hopefully it will remain this way :)

## I have a smart gadget and would like to help! {: #like-to-help }

Check if your gadget already has an implementation for one of a similar type/vendor. You could try to use [Pairing unsupported gadgets](../basics/pairing/index.md#pairing-unsupported-gadgets) which allows you to bypass the filter that distinguishes devices and you can add any device to Gadgetbridge as if it was one of the already supported devices. Then, look at older PRs and see if you can follow along some of the previous additions.

Look at how you can gather data from communication between the watch and the Android phone on which you first must run an app which can already talk to the watch.

For simple data collection without root see [collecting data with Wireshark](../internals/development/bluetooth.md#getting-live-bt_snoop-data-with-wireshark). The collected data can then be used for implementing support in Gadgetbridge, for that see the following articles:

* [Supporting a new gadget](../internals/topics/support.md)
* [Inspect Bluetooth packets](../internals/development/bluetooth.md)
* [New gadget tutorial](../internals/development/new-gadget.md)

## Can you help me to build my own app? {: #build-my-app }

No, sorry, we do not have capacity for that.
