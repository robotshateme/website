---
title: Calls and Replies
---

# Calls and Replies

While your gadget is connected, incoming calls and messages to your Android device will be sent to the gadget. 

Additionally, if your gadget supports replying to a message, and the notification sent from a messaging app that has a "Reply" button in the notification, you can set up canned (quick) replies in Gadgetbridge, so you can reply messages to directly from your gadget.

These canned replies will also be shown when rejecting the incoming call with a message, if supported by gadget.

<div class="gb-figure-container" markdown="span">
    <div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/demo/demo_mi_band_message.png){: height="300" style="height: 300px;" }
        Notification<br>with reply button
    </div><div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/demo/demo_mi_band_call.png){: height="300" style="height: 300px;" }
        Incoming call with<br>option to reply with SMS
    </div><div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/demo/demo_mi_band_replies.png){: height="300" style="height: 300px;" }
        Pick a canned reply<br>from available options
    </div>
</div>

## Setting up

<div class="gb-step" markdown>
![](../../assets/static/screenshots/sidebar_notifications.jpg){: height="600" style="height: 600px;" }
<div markdown>

To access the notification settings, tap the **:material-menu: Menu** icon or swipe from the left side to the right on the home screen to access the sidebar, then tap **:material-bell-outline: Notification settings**.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/notification_settings_2.jpg){: height="600" style="height: 600px;" }
<div markdown>

There are several settings that allow you to tweak the handling of incoming calls and messages. For example you can choose whether incoming calls should be signaled to your gadget, choose when incoming SMS should cause notifications on your gadget: always, only when your Android device's screen is off, never and so on. You can also set minimum time between notifications.

</div></div>

### Canned replies

<div class="gb-step" markdown>
![](../../assets/static/screenshots/device_replies.jpg){: height="600" style="height: 600px;" }
<div markdown>

Under "Device specific settings", find "Replies" and set canned replies for each slot. Then, tap on "Update on device" to write your canned replies to the gadget.

For Pebble gadgets, you may need to enable "Untested features" in gadget specific settings first as explained in {{ 203|issue }}.

</div></div>
