---
title: Synchronize data
---

# Synchronize data

To synchronize activity data from the gadget to Gadgetbridge, press the **:material-refresh: Sync** icon under your gadget name on the home screen. Some gadgets send this data to your phone automatically (like Pebble) so the sync icon is not present for these gadgets.

!!! warning "Warning"
    In order to receive data, make sure that no other app is syncing and removing the data from your gadget. Therefore make sure that the official app is not running. Kill the app (this might be not possible as every notification might start it up again) or uninstall it.
