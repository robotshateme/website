---
title: Sports and GPS
---

# Sports and GPS

Some gadgets can record "workouts" ("activities", "trainings"...). To create these recorded activities, you must start the recording on the gadget via its menu. These can then be synchronized with Gadgetbridge. If you do not synchronize these activities for a long time, the gadget might stop being able to record more data and will show a "Your data is full" message (or a similar one).

To view your workout history, make sure to [Synchronize data](./sync.md) first. Then press on **:material-run: Sports** icon from home screen to access activity charts.

## Sport activities

<div class="gb-step" markdown>
![](../../assets/static/screenshots/sports_activity_list.jpg){: height="600" style="height: 600px;" }
<div markdown>

Here you can manage your activities. Activities that contain GPS data will be indicated with :material-crosshairs-gps: icon next to the activity duration.

Tapping on an Activity opens up a details screen, long tap allows multi-selection and also "Delete" or "Share the GPX file" (if the activity contains it). You can also share the GPX later from the "Sports Activity Detail" screen.

</div></div>

!!! warning "Workouts needs to be manually synced"
    Sport activity syncing is always manual and it is not part of the automatic syncing. This allows you to sync your activities when you need it (rather then when the app decides), because it can take quite some time due to their size and some gadgets are not usable during the sync.

    Press the **:material-refresh: Refresh** button or swipe down to retrieve workouts from the gadget. Please note that only **one workout will be fetched at a time,** you have to sync again to get the next activity and so on.

    Gadgetbridge remembers the time of last synced item so if the workout data is still in the gadget (some gadgets erase it after transfer), you can re-fetch the data anytime later by using top-right menu ":material-menu: → Reset fetch time", setting the time back (to date before this activity happened) and re-syncing.

### Filtering

<div class="gb-step" markdown>
![](../../assets/static/screenshots/sports_filter.jpg){: height="600" style="height: 600px;" }
<div markdown>

Tap the **:material-filter: Filter** icon to show only activities you would like to see.

</div></div>

### Activity detail

<div class="gb-step" markdown>
![](../../assets/static/screenshots/sports_activity_details.jpg){: height="600" style="height: 600px;" }
<div markdown>

This shows details about the activity. If you long tap on the activity icon (in this case :material-walk:), the values will switch from recalculated (for example min/km) to raw, as provided by the band (for example sec/m). This can be useful for troubleshooting.

You can also click **:material-pencil: Edit** on top-right to edit the name of the activity.

</div></div>

## Heart rate

<div class="gb-step" markdown>
![](../../assets/static/screenshots/device_specific_settings_hr.jpg){: height="600" style="height: 600px;" }
<div markdown>

If supported by gadget, you might want to enable whole day heart rate measurement under "Device specific settings → Heart Rate Monitoring".

So the band will take measurements at least once in this interval. If an activity is detected, more frequent measurements are taken, capturing the entire activity in detail. So it is often sufficient to use the "Once an hour" interval and the gadget will capture all your active time, plus one sample per hour during idle time.

</div></div>

## GPS

Gadgets can be separated to three groups when it comes to the GPS.

### Built-in GPS receiver

When using a gadget that has a built-in GPS receiver, often you don't need to configure anything. 

However, these gadgets may use a system called [:material-wikipedia: Assisted GPS](https://en.wikipedia.org/wiki/Assisted_GNSS){: target="_blank" } to improve GPS reliability by providing the orbital information of satellites beforehand. If such a gadget has recent aGPS data, the GPS receiver performs much better. See [Huami GPS](../../internals/topics/huami-gps.md) to learn how to obtain and sync aGPS data for your gadget.

### Requesting GPS from phone

When using a gadget that doesn't have a built-in GPS receiver, but supports receiving location from your phone, Gadgetbridge sends your phone's location to the gadget automatically when requested by your gadget.

<div class="gb-step" markdown>
![](../../assets/static/screenshots/device_specific_settings_3.jpg){: height="600" style="height: 600px;" }
<div markdown>

For this, enable "Send GPS during workout" under "Device specific settings". 

Now, when you start the workout on the gadget, it will receive GPS from your phone. Make sure to enable the **:material-crosshairs-gps: GPS** location feature on your phone before starting a workout.

</div></div>

### Without GPS

When using a gadget that doesn't have a built-in GPS receiver **and** doesn't have the capability to request it from your phone, the recorded sports activities will be saved without location data. However, you can still use [3rd party applications to attach location data to your sport activities](../integrations/sports.md) at any time.

Some gadgets (like Fossil) send a signal to Gadgetbridge that a GPS workout is started or stopped. Gadgetbridge then uses the [OpenTracks API](../../internals/topics/opentracks-api.md) to send a signal to OpenTracks (if installed) to start or stop GPS tracking. While this doesn't fully integrate with Gadgetbridge sports activities, it still provides watch-controlled GPS tracking and on-watch statistics.

## GPX tracks

Sport activities that contain GPS data can be saved as "GPX" files. [GPX](https://www.topografix.com/gpx.asp){: target="_blank" } is a common GPS data format that can be used to define waypoints, tracks, locations, routes and more. As this data format is not specific to Gadgetbridge, a GPX file can be created, viewed and manipulated in other applications.

### File location

Synced GPX tracks from the gadget will be saved to Gadgetbridge's own [data folder](../../internals/development/data-management.md#data-folder-location). See the relevant link to find the GPX file on your phone, or later view on the desktop with [GPXSee](https://www.gpxsee.org/){: target="_blank" } or similar software.

### Viewing tracks

<div class="gb-step" markdown>
![](../../assets/static/screenshots/sports_activity_details_menu.jpg){: height="600" style="height: 600px;" }
<div markdown>

To open the recorded GPX track, tap on a sport activity, then the **:material-share-variant: Share** icon, and lastly "Show GPS Track". Now choose an installed application on your phone that is capable of viewing GPX tracks.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/osmand_view_gpx.jpg){: height="600" style="height: 600px;" }
<div markdown>

For example, [OsmAnd](https://f-droid.org/packages/net.osmand.plus/){: target="_blank" } is one of the map & navigation applications that support viewing GPX tracks with displaying every recorded detail.

There are many more FLOSS Android applications to view the recorded GPX file. Some examples are:

* [OpenTracks](https://f-droid.org/packages/de.dennisguse.opentracks){: target="_blank" }
* [OSM Dashboard for OpenTracks](https://f-droid.org/packages/de.storchp.opentracks.osmplugin/){: target="_blank" }
* [OSM Dashboard Offline for OpenTracks](https://f-droid.org/packages/de.storchp.opentracks.osmplugin.offline/){: target="_blank" }
* [FitoTrack ](https://f-droid.org/packages/de.tadris.fitness){: target="_blank" } - not via sharing icon, but by using import function in FitoTrack.
* [Another Android Tracker](https://f-droid.org/packages/ch.bailu.aat/){: target="_blank" } - seems like more precise representation of time and distance, calculates calories.
* [Milkha](https://apt.izzysoft.de/fdroid/index/apk/com.tensorfoo.milkha){: target="_blank" }

</div></div>

### Linking with activities

If you are using a gadget that doesn't have a built-in GPS chip and doesn't have the capability receiving GPS data from your phone, the sport activities will be recorded without GPS/GPX data by default. However, if you have a GPX file for an activity, you can link it to a sport activity at any time.

Also, some gadgets might not record GPS for some activities that started on the gadget by design such as indoor cycling, rowing machine, pool swimming, so having GPS linked for these activities can make sense.

<div class="gb-step" markdown>
![](../../assets/static/screenshots/sports_activity_details_gpx_link.jpg){: height="600" style="height: 600px;" }
<div markdown>

To link a GPX track with a sport activity, click **:material-circle-edit-outline: Edit GPX** on top-right next to the :material-pencil: pencil icon.

This will list all GPX tracks that are saved in Gadgetbridge. If you want to import a GPX file that is recorded from an external application, see [Importing to Gadgetbridge](../integrations/sports.md#importing-to-gadgetbridge).

</div></div>

## Limitations

Please be aware that not every detail of a given workout is currently being displayed in Gadgetbridge but the full workout is retrieved and stored. So as interpreting improves, the displayed data improves too. Interpreting the data is a challenge, so if you spot an issue, please try to figure out what the correct representation might be and report it on our [issue tracker](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues).

<!-- ![](../../assets/static/screenshots/device_specific_settings_2.jpg){: height="600" style="height: 600px;" } -->

*[FLOSS]: Free/Libre Open Source software
