---
title: Xiaomi protobuf watches
---

# Xiaomi protobuf watches

The following list includes all implemented features for Xiaomi protobuf devices. These are called protobuf devices because they use protobuf for the protocol, unlike other Huami devices. The actual supported features can vary for each device.

!!! warning "Experimental"
    Support for these devices is recent, and still experimental. This page is also still a work in progress and not complete.

List of supported Xiaomi gadgets by Gadgetbridge which use protobuf:

<!--
    This is an auto-populated list from gadgets which has "os_xiaomi_protobuf" flag,
    see "device_support.yml" file for more details.

    It basically creates a list item for every gadget and 
    changes the casing for setting as display name for links.
    (like "xiaomi_mi_band_1" becomes to "Xiaomi Mi Band 1")

    Then, an anchor ID appends to the end of the page link,
    so when clicked on it, it will point to the related gadget's own section.
-->
{% for key, device in device_support.items() | sort %}
    {%- if "os_xiaomi_protobuf" in device.flags %}
        {% set name = key.replace("_", " ").title() %}
        {% set icon = ' :material-flask-empty-outline:{: title="Experimental" }' if "experimental" in device.flags else "" %}
* [{{ name }}](../../gadgets/wearables/{{ device.vendor }}.md#device__{{ key }}) {{ icon }}
    {% endif -%}
{% endfor %}

### Implemented features

These features are supported by Gadgetbridge and apply to all Xiaomi watch models included in this page. Note that actual available features per device depends on the device capabilities.

??? note "List of features supported by Gadgetbridge"

    - Authentication / Encryption
    - Set time (date, time, timezone offset, dst offset, 12h/24h)
    - Device info (firmware, model, serial)
    - Device state (battery, sleep, wearing)
    - Music (info, state, media buttons from watch)
    - Notifications
        - Apps, calls, custom icons, open on phone, dismiss on phone and watch
        - Ignore and reject calls
        - Reject calls with SMS
        - Configure canned messages for calls
        - Emoji
    - Contacts upload to watch
    - Alarms
    - Reminders
    - Flashing (watchface, firmware)
    - Watchface management
    - Workouts
        - Start fitness app on phone
        - Send GPS to watch
    - Find phone and watch
    - Calendar events sync
    - Activity sync (partial, see below)
    - Settings
        - Language
        - Wearing Mode (band / pebble / necklace)
        - User Info (height, weight, birthday, gender)
        - Goals (calories, steps, standing time, active time)
        - Goal notification (enabled, secondary goal)
        - Vitality score settings (7 day reminder, daily reminder)
        - Heart rate (sleep detection, sleep breathing quality, interval, high and low alerts)
        - SpO2 (all-day, low alert)
        - Stress (enable, relaxation reminder)
        - Standing reminder (start and end times, dnd period)
        - Sleep mode schedule
        - Screen on on notifications
        - Camera remote
        - Password
        - Display items
    - Realtime stats (steps, heart rate)
    - Weather
    - Phone silent mode from watch
    - Widgets

### Activity Sync

Activity sync is very experimental. It was mainly tested on the Mi Band 8, and may present issues on other devices.

Implemented:

- Heart rate, steps
- SpO2
- Stress
- Vitality Score (equivalent to PAI)
- Sleep times and sleep stages

Missing:

- Calories (fetched, but no UI)
- Moving time
- Standing time (fetched, but no UI)
- VO2 Max
- Blood pressure
- Body temperature (Watch S1 Pro)
- Training load (fetched, but no UI)
- Sleep respiratory rate
- Heart rate stats (resting, min, max, average - fetched, but no UI)
- SpO2 stats (min, max, average - fetched, but no UI)
- Workouts (partially implemented)

### Known issues

!!! note "Open issues"
    Do not forget to check any other [open issues](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues?q=&type=all&sort=&state=open&labels=154786%2c-1238&milestone=0&project=0&assignee=0&poster=0) in the project repository.

- Weather is potentially incomplete
    - Multiple weather locations not implemented
- Wearing mode must be changed at least once from the watch for the preference to show in Gadgetbridge
- A toast about an error syncing calendar database sometimes appears
- Sleep stages do not work for all devices

### Missing features

- Some settings:
    - Change the app layout
    - Raise to wake sensitivity and schedule
    - Manage Workouts
    - Vibration patterns
    - DND Sync
    - World Clocks
- Firmware parsing (they can be installed, but must be set in the code)
- Cycles
- Send phone alarms to watch
