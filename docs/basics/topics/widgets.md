---
title: Home screen widgets
---

# Home screen widgets

There are two widgets in Gadgetbridge:

* [Zzz widget](#zzz-widget) for quick alarm setting.
* [Main widget](#main-main) which shows current step count, sleep hours, status and allows alarm settings too.

## Zzz widget

Upon tapping the Zzz, this widget quickly sets an alarm after "Preferred sleep duration" as set in "Preferences → About you". This alarm is set by overwriting the first alarm on the device.

## Main widget

It shows current device name, total step count, total distance (steps * step length), sleep hours (since the previous day's noon) and battery status/level (not connected/connecting/battery percentage). 

The widget provides quick access to several actions:

* Tap on the gadget name requests data re-sync. If gadget is currently not connected via Bluetooth, Gadgetbridge attempts to reconnect first.
* Clock icon opens alarm popup to quickly set an alarm for 5 mins, 10 mins, 20 mins, 1 hour, 8 hours (Last item uses hours value as is set in your "Preferences → About you → Preferred sleep duration"). This alarm is set by overwriting the first alarm on the device.
* Gadgetbridge icon opens the Gadgetbridge app.
* Steps/distance/sleep opens up the Activity charts in Gadgetbridge.
* Long tap the widget to resize it.
