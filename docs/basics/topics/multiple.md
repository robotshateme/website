---
title: Multiple gadgets or phones
---

# Multiple gadgets or phones

## Multiple Android devices connecting to one gadget

It is possible to share one gadget among multiple Android devices, but only one Android device can be connected at a time. Note that by default Gadgetbridge acknowledges the activity data sent by the device (i.e. the device will remove this data from its internal memory) and you won't have a complete set on any Android device. You also need a gadget that supports pairing to multiple devices, as far as we know Mi Band does not support this scenario while Pebble and Fossil gadgets do.

### Pebble

In the Pebble settings, starting with release 0.10.1, the activity trackers to sync can be set by checking the related checkboxes in Pebble settings. This is independent from the activity tracker to display on graphs (currently labeled "Preferred Activity Tracker") but activity trackers that are not synced will save no data in Gadgetbridge's database, hence nothing will be shown on the graphs either.

The default Gadgetbridge configuration is for a single, "Master" device having all the checkboxes checked. If you want to use another device preserving your activity data, configure this "Slave" Android device ensuring the checkboxes that are relevant to you are not selected. Be aware that by default these checkboxes are checked, because that's the most common use-case for Gadgetbridge.

#### Multiple trackers

You can use multiple trackers simultaneously (e.g. "Pebble Health" and "Morpheuz"). By default, sync for all supported trackers is activated - which should be fine for most users (even if they don't have Morpheus or Misfit installed). The only context in which you must deactivate them as [described above](#multiple-android-devices-connecting-to-one-gadget).

If you are satisfied with a third party tracker and want to switch off the integrated Pebble Health, you can do so from the app manager: long-press on the "Health" app and tap "Deactivate" (at the same place you can press "Activate" to enable it). For Morpheuz and Misfit, simply uninstall their watch apps if you no longer wish to use them.

Note that at least [Pebble discourages using multiple tracking apps simultaneously.](https://help.getpebble.com/customer/en/portal/articles/2239065-pebble-health?b_id=8308){: target="_blank" }

> Pebble Health is very efficient and should use less battery than other third party fitness trackers. Please note, running two activity trackers will increase battery drain, and is not recommended.

<!-- TODO: Maybe move section to dedicated page? -->

## One Android device connecting to multiple gadgets

As of Gadgetbridge 0.68.0, it is possible to connect to multiple gadgets. Most issues with this support have been ironed out, but if you encounter anything out of the ordinary, please report it on our [issue tracker](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues).
