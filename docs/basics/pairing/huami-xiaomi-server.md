---
title: Huami/Xiaomi server pairing
---

# Huami/Xiaomi server pairing

New Amazfit and Xiaomi gadgets and older gadgets with updated firmware require server based pairing. This means, that you absolutely **must use** the original Mi Fit / Amazfit / Zepp app to make the initial pairing, retrieve the pairing key and then use this key to pair with Gadgetbridge.

!!! abstract "Usage of non-free (as-in freedom) software ahead"
    Unfortunately, these steps will take you outside of Gadgetbridge and require you to create an account with your gadget's vendor's website and/or download a proprietary app.

    As the authentication keys are stored on vendor servers, there is no way to generate these keys locally at the moment.

The authentication key is stored on Huami servers or in the original Mi Fit / Mi Fitness / Xiaomi Wear / Zepp app's database. There are several ways to obtain the key.

<!-- TODO: maybe make a simple JavaScript form to pull token from servers in the browser directly? -->

## Older devices without encryption

Some old Xiaomi devices use the Xiaomi account ID as authentication key. For these, you can just use the numeric account ID, as seen in the official app next to the account name. These devices are:

- Mi Watch Color Sport
- Mi Watch Lite

If you have one of these devices, continue to the next section.

[:material-arrow-down: Entering key](#entering-key){: .gb-button }

## On rooted phones

Install Zepp Life or Zepp vendor app depending which app supports your gadget. Then create your account and pair your gadget. After that, quit the vendor app (don't uninstall it yet), and run the appropriate command for your gadget in a root shell. (e.g. Termux for on-device, or ADB for on-computer)

Replace `[YOURDBNAMEHERE]` with the name of the database file of vendor app. You can use a file explorer to get the name of the database file name.

=== "Zepp Life"
    ```
    sqlite3 /data/data/com.xiaomi.hm.health/databases/origin_db_[YOURDBNAMEHERE] "select AUTHKEY from DEVICE"
    ```

=== "Zepp (formerly Amazfit)"
    ```
    sqlite3 /data/data/com.huami.watch.hmwatchmanager/databases/origin_db_[YOURDBNAMEHERE] "select AUTHKEY from DEVICE"
    ```

=== "Mi Fitness (Xiaomi Wear)"
    ```
    sqlite3 /data/data/com.xiaomi.wearable/databases/device_db "select detail from device"
    ```

Within the obtained JSON, the attribute `auth_key` contains the key. Note that the key name might be different for some devices not manufactured by Huami, such as `encrypt_key` as explained in {{ 3146 | issue(1360513) }}.

Now, keep that key in your clipboard and jump to the next section to continue.

[:material-arrow-down: Entering key](#entering-key){: .gb-button }

## On non-rooted phones

This section only applies to devices manufactured by Huami.

### Huami token

This section only applies to devices manufactured by Huami.

The full process is not covered here, but you can try these projects to obtain the authentication key from Huami servers by logging into your Xiaomi or Amazfit account.

* [huafetcher](https://codeberg.org/vanous/huafetcher)
* [huami-token](https://codeberg.org/argrento/huami-token)

### Mi Fitness / Xiaomi Wear

If your device is supported by the Mi Fitness app, there are reports that it prints the `authKey` in the logs ({{ 2991|issue }}), which are available in the phone's internal storage. Accessing the logs does not require root and the path to the relevant file is `/sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log`

Assuming you've enabled USB debugging on your mobile device, you can obtain the key from logs on your device with `adb` shell.

``` bash
grep -E "authKey=[a-z0-9]*," /sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log | \
    awk -F ", " '{print $17}' | grep authKey | tail -1 | awk -F "=" '{print $2}'
```

It seems that in the latest versions of Mi Fitness (+3.20.2i) the field `authKey` is null so you have two more options with the fields `token` or `encryptKey`:

=== "`encryptKey`"
    ```bash
    grep -E "encryptKey=[a-z0-9]*," /sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log | awk -F ", " '{print $12}' | grep encryptKey | tail -1 | awk -F "=" '{print $2}'
    ```

=== "`token`"
    ```bash
    grep -E "token=[a-z0-9]*," /sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log | awk -F ", " '{print $11}' | grep token | tail -1 | awk -F "=" '{print $2}'
    ```

This process can be done within [Termux](https://termux.dev/en/){: target="_blank" } on the same device if you can access to `/sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log`. It seems that in custom ROMs is possible but not in some stock ROMs like in the Pixel's. If you are able to access it, you can copy the log to a more comfortable location and run a command like the followings:

=== "`encryptKey`"
    ```bash
    grep -E "encryptKey=[a-z0-9]*," ~/downloads/XiaomiFit.device.log | awk -F ", " '{print $12}' | grep encryptKey | tail -1 | awk -F "=" '{print $2}'
    ```

=== "`token`"
    ```bash
    grep -E "token=[a-z0-9]*," ~/downloads/XiaomiFit.device.log | awk -F ", " '{print $11}' | grep token | tail -1 | awk -F "=" '{print $2}'
    ```

There is a report that getting the authentication key fails in some cases. See [this issue](https://codeberg.org/argrento/huami-token/issues/66){: target="_blank" } for more details.

[:material-arrow-down: Entering key](#entering-key){: .gb-button }

---

## Entering key

--8<--
pairing.md:auth_key_first
pairing.md:auth_key_hex
pairing.md:auth_key_finish
--8<--

!!! warning "Don't unpair from vendor app"
    Anytime you unpair your gadget from Mi Fit / Amazfit / Zepp the pairing key will be invalidated and you must follow these steps again to grab the newly generated key. Just uninstall the vendor app.

!!! warning "Hard resetting the gadget invalidates the key"
    Every time you hard reset the gadget, the Bluetooth MAC address will be changed and you must grab a new key.

## Troubleshooting

If you encounter problems with Bluetooth discovery or pairing process, try the following steps:

* Make sure to have first paired in Mi Fit / Amazfit / Zepp vendor app.
* Make sure these vendor apps is not running at the moment.
* Make sure **you did not unpair** your gadget from vendor app.
* Make sure to add `0x` in from your key when pasting/typing it to Gadgetbridge.
* Make sure `0x` is really `0` (zero) and `x` (lowercase X), since some keyboards might use different symbols, see {{ 1775|issue(47662) }}.
* Do not press Enter/Return key when typing the key, which may cause adding a new line.
* Make sure that gadget is unpaired from the phone's Bluetooth.
* Toggle Bluetooth OFF and ON.
* Reboot your smartphone.
* Reboot your gadget (More → Settings → Reboot).
