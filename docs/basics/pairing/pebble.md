---
title: About Pebble gadgets
---

# About Pebble gadgets

All Pebbles when powered up for the first time demand that you connect it via the official App. **This is not necessary, you can get started with Gadgetbridge only**. While on original Pebbles this screen could be bypassed by a long press on the right button, newer pebbles seem to be stuck in recovery until a firmware gets flashed.

We recommend to flash the latest available firmware. On the Pebble and Pebble Steel (black and white models), this includes flashing a migration firmware first.

See also the [Pebble](../../gadgets/wearables/pebble.md) gadget page for related links and resources for Pebble.

Steps to follow:

* Install Gadgetbridge
* Pair your Pebble through Gadgetbridge's Control Center, **do NOT select THE Pebble LE!**
* Start Gadgetbridge and tap on your Pebble until it says initialized
* Flash a firmware as noted in [Pebble Firmware Update](../../internals/specifics/pebble.md)
* Install a language pack as noted in [Pebble Language Packs](../../internals/specifics/pebble-language.md)
* Optionally enable Health if device supports it <!-- TODO: see [[Pebble System Apps#health-not-for-pebble-classic-and-steel]]. -->

!!! warning "Connection problems"
    Pebble pairing can be very frustrating and there are many issues in our issue tracker about it. Recommendations vary from "You should NOT pair a BTLE device via Android's Bluetooth settings" to "Connect Pebble through Android Settings"

## Pairing a Pebble 2

Instructions copied from {{ 2118|issue(237690) }} but reduced to the necessary steps.

Make sure Pebble related setting in Gadgetbridge are set as defaults. If you are not sure, backup your data and delete all data from Gadgetbridge to ensure clean settings. Make sure to have "ignore bonded devices" disabled. [Companion device pairing](./companion-device.md) can be enabled for additional functions.

* Connect Pebble through Android Settings
* Turn Bluetooth on the Pebble off → Remove the phone from the Pebble → Turn Bluetooth on again
* Open Gadgetbridge and press the :material-plus-box: **Add** button to add a device
* Click on the Pebble (marked as already connected)
* There should be a toast on the phone to confirm, followed by the pair screen on the Pebble and after confirming the Pebble should be visible in Gadgetbridge

## Pairing a Pebble BTLE

Instructions quoted from {{ 1098|issue(41052) }}.

You should **NOT** pair a BTLE device via Android's Bluetooth settings. If you already did so and ran into problems with it in Gadgetbridge (which you certainly will if you did), first step is to "forget the device" - that is the Pebble in Android's Bluetooth settings as well as the Android device in the Pebble's settings.

* Set/keep the "Client only" option enabled
* Forget the Pebble (on the Android device) and the Android device (on the Pebble)
* Toggle Bluetooth on both pebble and Android
* Add the Pebble again (pressing the + button in Gadgetbridge)

## Pairing a Pebble OG

* Disable "Companion device pairing" (experienced on Android 8)
* Pair via Gadgetbridge's :material-plus-box: **Add** button
* Do not select the LE Pebble but the "normal" Pebble in the list of found devices
