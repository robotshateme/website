---
title: Nothing CMF server pairing
---

# Nothing CMF server pairing

The Nothing CMF watches require server based pairing. This means, that you absolutely **must use** the original CMF Watch app to make the initial pairing, retrieve the pairing key and then use this key to pair with Gadgetbridge.

!!! abstract "Usage of non-free (as-in freedom) software ahead"
    Unfortunately, these steps will take you outside of Gadgetbridge and require you to create an account with your gadget's vendor's website and/or download a proprietary app.

    As the authentication keys are stored on vendor servers, there is no way to generate these keys locally at the moment.

The authentication key is stored in the original CMF Watch app's database. Currently, the only known way to obtain the key requires a rooted phone.

## On rooted phones

Install the CMF Watch app. Then create your account and pair your gadget. After that, quit the vendor app (don't uninstall it yet), and run the appropriate command for your gadget in a root shell. (e.g. Termux for on-device, or ADB for on-computer)

```
sqlite3 /data/data/com.nothing.cmf.watch/com.nothing.cmf.watch/databases/ntwatch.db "SELECT K1 FROM DeviceConfig"
```

Now, keep that key in your clipboard and jump to the next section to continue.

[:material-arrow-down: Entering key](#entering-key){: .gb-button }

---

## Entering key

--8<--
pairing.md:auth_key_first
pairing.md:auth_key_finish
--8<--

!!! warning "Factory resetting the gadget invalidates the key"
    Every time you factory reset the gadget, the key will also be reset, and you will have to grab a new one.

## Troubleshooting

If you encounter problems with Bluetooth discovery or pairing process, try the following steps:

* Make sure to have first paired in CMF Watch app.
* Make sure these vendor apps is not running at the moment.
* Make sure **you did not unpair** your gadget from vendor app.
* Do not press Enter/Return key when typing the key, which may cause adding a new line.
* Make sure that gadget is unpaired from the phone's Bluetooth.
* Toggle Bluetooth OFF and ON.
* Reboot your smartphone.
* Reboot your gadget (More → Settings → Reboot).
