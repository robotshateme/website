---
title: Xiaomi
---

# Xiaomi

Mi Bands usually have fairly accurate step and heart rate sensors. Total sleep hours are measured and info is provided but sleep cycles (light/deep sleep) are not accurately discovered, but the heart rate pattern allows you to observe your sleep patterns very well. Support in Gadgetbridge includes workout tracking and also workouts that require GPS.

!!! success "Good support"
    Mi Bands have gained a lot of popularity and have good support in Gadgetbridge.

## Mi Band 1 {{ device("xiaomi_mi_band_1") }}

It looks like it is not feasible to have both Mi Fit (now Zepp Life) and Gadgetbridge installed at the same time. {{ 330|issue }} suggests that Mi Fit (now Zepp Life) restarts itself and automatically connects to your Mi device, preventing Gadgetbridge from functioning properly.

Maybe it is sufficient to deactivate Mi Fit (now Zepp Life) instead of uninstalling it completely.

The first gen Moto G (2013) works fine with the Mi Band when updated to Android 5.1 (most should be), tested with Mi Band firmware 04.15.12.10. Everything seems to work.

Other versions like Android 4.4.x (KitKat) may have problems with the Mi Band - occurred with Mi Band firmware versions:

* 1.4.0.3 (everything in Gadgetbridge works **except** fetching activity data)
* 1.0.9.14 (nothing works, cannot even connect properly)

## Mi Band 1A {{ device("xiaomi_mi_band_1a") }}

Given details under [Mi Band 1](#mi-band-1) also apply to this gadget.

## Mi Band 1S {{ device("xiaomi_mi_band_1s") }}

Given details under [Mi Band 1](#mi-band-1) also apply to this gadget.

## Mi Band 2 {{ device("xiaomi_mi_band_2") }}

This gadget acts almost like the [Amazfit Bip](amazfit.md#bip), and almost all features that work on the Mi Band 2 already work for Amazfit Bip.

Supported features by Gadgetbridge:

* Rejecting phone calls
* Ignoring calls
* Weather forecast
* Full text notifications with icons
* Export of activities with GPS and HR data
* Flash new watchfaces (with the firmware update tool)
* Use button for actions
* Reboot (debug menu)

To display text notifications, you [might need to install a font](../../internals/specifics/mi-band-2.md#font-format), as those are sometimes not installed on the device by default. This process is identical to the firmware update process which is linked above. After extracting the APK, select the `Mili_pro.ft.en` file instead.

Some users reported that after initial adding to Gadgetbridge, using the "Disable Bluetooth Pairing" option was important to actually allow the connection to happen.

## Mi Band 3 {{ device("xiaomi_mi_band_3") }}

## Mi Band 4 {{ device("xiaomi_mi_band_4") }}

!!! warning "Not related with Mi Band 4C!"
    The Mi Band 4C is a completely different device, and is **NOT** supported by Gadgetbridge. See {{ 2020|issue }} for details.

## Mi Band 5 {{ device("xiaomi_mi_band_5") }}

Essentially the same as [Amazfit Band 5](./amazfit.md#band-5) but without the extra VO2 max sensor.

## Mi Band 6 {{ device("xiaomi_mi_band_6") }}

If you get a "Update the app to latest version" message on the band, make sure to check the "New Auth Protocol" in the device settings in Gadgetbridge.

If you are pairing the device for the first time, you can get to the Setting screen by long press on the device in the Discovery screen.

<!-- TODO: add images -->

## Mi Band 7 {{ device("xiaomi_mi_band_7") }}

Also known as Xiaomi Smart Band 7.

Tested with firmware versions: 1.19.1.5, 1.20.3.1, 1.27.0.4, 2.0.0.2, and hardware versions: 0.91.177.3.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Mi Band 7 Pro {{ device("xiaomi_mi_band_7_pro") }}

Also known as the Xiaomi Smart Band 7 Pro. Not related with the Mi Band 7, and in fact uses a completely different protocol.

Added based on feedback from {{ 2781|issue }}.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Mi Band 8 {{ device("xiaomi_mi_band_8") }}

Also known as the Xiaomi Smart Band 8. Not related with the Mi Band 7, and in fact uses a completely different protocol.

Tested with firmware versions: 2.1.8, 2.2.12, and hardware versions: M2239B1.

Most of the Xiaomi protobuf implementation was created for the Mi Band 8, so it is highly supported.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Mi Band 8 Pro {{ device("xiaomi_mi_band_8_pro") }}

Also known as the Xiaomi Smart Band 8 Pro.

Untested, added based on feedback from {{ 3471|issue }}.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Mi Watch Color Sport {{ device("xiaomi_mi_watch_color_sport") }}

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Mi Watch Lite {{ device("xiaomi_mi_watch_lite") }}

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Redmi Smart Band 2 {{ device("xiaomi_redmi_smart_band_2") }}

Added based on feedback from {{ 3274|issue }}.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Redmi Smart Band Pro {{ device("xiaomi_redmi_smart_band_pro") }}

Added based on feedback from {{ 3069|issue }}.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Redmi Watch 2 {{ device("xiaomi_redmi_watch_2") }}

Added based on feedback from {{ 3543|issue }}.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Redmi Watch 2 Lite {{ device("xiaomi_redmi_watch_2_lite") }}

Added based on feedback from {{ 2637|issue }}.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Redmi Watch 3 {{ device("xiaomi_redmi_watch_3") }}

Support added on {{ 3582|pull }}.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Redmi Watch 3 Active {{ device("xiaomi_redmi_watch_3_active") }}

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Watch S1 {{ device("xiaomi_watch_s1") }}

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Watch S1 Active {{ device("xiaomi_watch_s1_active") }}

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Watch S1 Pro {{ device("xiaomi_watch_s1_pro") }}

Added based on feedback from {{ 3450|issue }}.

Known issues:

* sleep stages are not working.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.

## Watch S3 {{ device("xiaomi_watch_s3") }}

Not working - see {{ 3506|issue }}.

!!! tip "Xiaomi protobuf device"
    See the [Xiaomi protobuf](../../basics/topics/xiaomi-protobuf.md) for a list of features and issues common to Xiaomi protobuf watches.
