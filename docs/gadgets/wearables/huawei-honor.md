---
title: Huawei/Honor
---

# Huawei/Honor

See the [Huawei/Honor gadgets](../../basics/topics/huawei-honor.md) for a list of features and issues common to all Huawei and Honor watches.

## Honor Band 3 {{ device("honor_band_3") }}

## Honor Band 4 {{ device("honor_band_4") }}

## Honor Band 5 {{ device("honor_band_5") }}

## Honor Band 6 {{ device("honor_band_6") }}

## Honor Band 7 {{ device("honor_band_7") }}

Support for this band is still experimental and not yet confirmed to be working.

## Honor MagicWatch 2 {{ device("honor_magicwatch_2") }}

Support for this watch was added in {{ 3541|pull }}.

## Huawei Band 3e {{ device("huawei_band_3e") }}

## Huawei Band 4 {{ device("huawei_band_4") }}

## Huawei Band 4 Pro {{ device("huawei_band_4_pro") }}

## Huawei Band 4e {{ device("huawei_band_4e") }}

## Huawei Band 6 {{ device("huawei_band_6") }}

## Huawei Band 7 {{ device("huawei_band_7") }}

## Huawei Band 8 {{ device("huawei_band_8") }}

Support for this watch is still very early and not working properly - see {{ 3495 | issue }}.

## Huawei Talk Band B6 {{ device("huawei_talk_band_b6") }}

## Huawei Watch GT {{ device("huawei_watch_gt") }}

## Huawei Watch GT 2 {{ device("huawei_watch_gt_2") }}

## Huawei Watch GT 2 Pro {{ device("huawei_watch_gt_2_pro") }}

Support for this watch is still very early and not working properly.

## Huawei Watch GT 2e {{ device("huawei_watch_gt_2e") }}

Support for this watch is still very early and not working properly.

## Huawei Watch GT 3 {{ device("huawei_watch_gt_3") }}

Support for this watch is still very early and not working properly.

## Huawei Watch GT 3 Pro {{ device("huawei_watch_gt_3_pro") }}

Support for this watch is still very early and not working properly.

## Huawei Watch Fit {{ device("huawei_watch_fit") }}

Support for this watch was added in {{ 3574|pull }}.
