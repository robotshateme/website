---
title: Others & unbranded
---

# Others & unbranded

## BFH-16 {{ device("other_bfh-16") }}

## Bohemic Smart Bracelet {{ device("other_bohemic_smart_bracelet") }}

A rebranded Lefun device.

## ID115 {{ device("other_id115") }}

## ID115 Plus {{ device("other_id115_plus") }}

May also be branded as "Lefun".

## JYou Y5 {{ device("jyou_y5") }}

## Makibes HR3 {{ device("makibes_hr3") }}

## NO.1 F1 {{ device("other_no_f1") }}

## TLW64 {{ device("other_tlw64") }}

## Teclast H10 {{ device("other_teclast_h10") }}

## Teclast H30 {{ device("other_teclast_h30") }}

## XWatch {{ device("other_xwatch") }}

Affordable Chinese Casio-like smartwatches.
