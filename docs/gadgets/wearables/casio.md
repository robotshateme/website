---
title: Casio
---

# Casio

Casio watch models that work with Gadgetbridge often have connection problems. In addition, due to the watch's operating logic, the data on the watch is overwritten with each reconnection.

With {{ 1377|pull }} an initial support has added for some Casio watches.

## GB-5600B {{ device("casio_gb-5600b") }}

The Casio GB-5600B, GB-6900B, GB-X6900B and STB-1000 watches are one of the first Casio models to be Bluetooth-enabled and the first Casio models to be supported by Gadgetbridge. Unfortunately, BLE communication within the watch itself is problematic, so your experience may vary.

??? warning "Connection problems"
    Connecting to the watch sometimes takes more than one try. If you enable automatic BLE reconnect, the process might be automatic. When connecting for the first time, be sure to delete the existing pairing from both, the phone and the watch, before adding it to Gadgetbridge.

    The very first connection, i.e. when pairing, does not provide any functionality. You have to manually disconnect and reconnect the watch to make it fully working. To be sure, disable and enable Bluetooth on the phone between the connection attempts in order to refresh the GATT characteristic cache.

    Casio watches tend to lose connection from time to time, especially when using LTE data very heavily. While the automatic BLE reconnect included in Gadgetbridge sometimes works, it is often not fast enough to find the Casio watch, since it advertises only for about 8 seconds before turning Bluetooth off. There is a {{ 1416|pull }} to improve reconnection but has not yet been merged. This situation has improved significantly and the PR is abandoned.

Supported features by Gadgetbridge:

* Notifications
* Music Control
* Phone Finder
* Time synchronization
* Alarm Configuration

Currently, the only adjustments to the watch are setting alarms and time synchronization. No further configuration is possible.

The watch seems to always require time synchronization on connecting. Thus, the respective option in Gadgetbridge's settings has no effect for Casio models, the time is always synchronized when requested by the watch.

## GB-6900B {{ device("casio_gb-6900b") }}

Given details under [GB-5600B](#gb-5600b) also apply to this gadget.

## GB-X6900B {{ device("casio_gb-x6900b") }}

Given details under [GB-5600B](#gb-5600b) also apply to this gadget.

## STB-1000 {{ device("casio_stb-1000") }}

Given details under [GB-5600B](#gb-5600b) also apply to this gadget.

Additionally, control mode is active directly after connection has been established. In order to leave this mode, long-press the "MODE" button on the watch.

## GBX-100 {{ device("casio_gbx-100") }}

The Casio GBX-100, GBD-200 and GBD-H1000 are the second Casio watch generation supported by Gadgetbridge. Please keep in mind that, due to its recent addition, not all features are available.

Make sure the delete the pairing information from the watch and the phone. If you have the Casio app installed, uninstall it before using Gadgetbridge. During the pairing process, the information stored in Gadgetbridge (your profile) is transferred to the watch and the information on the watch is overwritten. Also, the time is synchronized to the watch - this cannot be disabled and is a limitation of Casio's pairing protocol. Since Gadgetbridge only stored the year of birth, day and month are set to January 1. This is the only information that you can safely change on the watch without being overwritten by Gadgetbridge.

Supported features by Gadgetbridge:

* Profile Synchronization
* Setting step count and energy target (calculated based on the profile information)
* Notifications
* Phone finder

Currently work in progress:

* Step Counter

Low Priority:

* Exercise Data
* Heart rate Data (GBD-H1000 only)
* GPS Data (GBD-H1000 only)

No Priority:

* Tide Data (GBX-100 only)
* Sunset/Sunrise Data
* World Time

The synchronization is set up in the "Gadgetbridge-always-wins"-style. This means that all information changed on the watch will be overwritten by Gadgetbridge upon the next reconnect.

The units are currently not synchronized. Other units than metric units have not been tried, although this part of the protocol is fully understood.

## GBD-100 {{ device("casio_gbd-100") }}

Support for this device is untested. Given details under [GBD-200](#gbd-200) also apply to this gadget.

## GBD-200 {{ device("casio_gbd-200") }}

Given details under [GBX-100](#gbx-100) also apply to this gadget.

## GBD-H1000 {{ device("casio_gbd-h1000") }}

Given details under [GBX-100](#gbx-100) also apply to this gadget.

## GW-B5600 {{ device("casio_gw-b5600") }}

Initial support added with {{ 3218|pull }}.

## GMW-B5000 {{ device("casio_gmw-b5000") }}

Initial support added with {{ 3218|pull }}, but was not tested.
