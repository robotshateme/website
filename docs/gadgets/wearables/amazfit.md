---
title: Amazfit
---

# Amazfit

## Active {{ device("amazfit_active") }}

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

!!! warning "Do not upgrade firmware"
    Recent firmwares (>= 3.17.0.1) use a new protocol, which is not supported, and the watch will not be able to connect to Gadgetbridge - see the comments on {{ 3496 | issue }}.

## Active Edge {{ device("amazfit_active_edge") }}

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Balance {{ device("amazfit_balance") }}

!!! tip "Zepp OS 3.0 device"
    This is a Zepp OS 3.0 device, which is still in the early stages of development. See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Band 5 {{ device("amazfit_band_5") }}

Essentially the same as [Mi Band 5](./xiaomi.md#mi-band-5) but with extra VO2 max sensor.

## Band 7 {{ device("amazfit_band_7") }}

Tested with firmware versions: 1.10.31.1, and hardware versions: 0.99.21.5.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Bip {{ device("amazfit_bip") }}

This gadget acts almost like the [Mi Band 2](./xiaomi.md#mi-band-2), and almost all features that work on the Mi Band 2 already work for Amazfit Bip.

When powered up for the first time, the gadget demands that you connect in via the official vendor app. This is not necessary, you can get started with Gadgetbridge only.

Supported features by Gadgetbridge:

* Rejecting phone calls
* Ignoring calls
* Weather forecast
* Full text notifications with icons
* Export of activities with GPS and HR data
* Flash new watchfaces (with the firmware update tool)
* Use button for actions

## Bip 3 Pro {{ device("amazfit_bip_3_pro") }}

Support for this gadget was added in {{ 3249|issue }}, but is not extensively tested such as firmware and watchface updates.

## Bip 5 {{ device("amazfit_bip_5") }}

Support for this gadget was added in {{ 3258|pull }}, without access to the device. It should work, but was not tested at all. If you confirm it works or face any issues, please open an issue or reach out in Matrix.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Bip Lite {{ device("amazfit_bip_lite") }}

## Bip S {{ device("amazfit_bip_s") }}

The Amazfit Bip S is an improved Version of the original Amazfit Bip. It has a few hardware changes and software improvements:

* 64 (RGB222) instead of 8 colors (RGB111), however the new screen is not as readable in low-light environments as the old one, but looks much nicer when the background light is enabled than the original Bip screen.
* Music controls officially supported.
* Better HR Sensor.
* World clock (not yet supported in Gadgetbridge).
* Many more activities supported.
    * Including swimming.
* 5ATM water resistance.
* Bluetooth 5.0.
* (probably more)

There are a few protocol changes in comparison to the original Bip, and support in Gadgetbridge is not yet on par with the original Amazfit Bip. Bip S is working mostly in the same way regular [Amazfit Bip](#bip) does.

## Bip S Lite {{ device("amazfit_bip_s_lite") }}

## Bip U {{ device("amazfit_bip_u") }}

## Bip U Pro {{ device("amazfit_bip_u_pro") }}

## Cheetah {{ device("amazfit_cheetah") }}

Both round and square variations are supported.

Support for this gadget was added in {{ 3258|pull }}, without access to the device. It should work, but was not tested at all. If you confirm it works or face any issues, please open an issue or reach out in Matrix.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Cheetah Pro {{ device("amazfit_cheetah_pro") }}

Support for this gadget was added with {{ 3251|pull }}.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Cor {{ device("amazfit_cor") }}

This gadget uses the same system-on-chip like the [Amazfit Bip](#bip), and the firmware acts almost identically.

## Cor 2 {{ device("amazfit_cor_2") }}

This gadget is very similar to the [Amazfit Cor](#cor). We do not have the gadget at it is largely untested.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Falcon {{ device("amazfit_falcon") }}

Support for this gadget was added in {{ 3258|pull }}, without access to the device. It should work, but was not tested at all. If you confirm it works or face any issues, please open an issue or reach out in Matrix.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## GTR {{ device("amazfit_gtr") }}

## GTR 2/2e {{ device("amazfit_gtr_2") }}

## GTR 3 {{ device("amazfit_gtr_3") }}

Tested with firmware versions: 7.46.1.1, and hardware versions: 0.84.19.19.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## GTR 3 Pro {{ device("amazfit_gtr_3_pro") }}

Tested with firmware versions: 8.45.7.1, and hardware versions: 0.83.19.2.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## GTR 4 {{ device("amazfit_gtr_4") }}

Tested with firmware versions: 3.8.5.1, 3.17.0.2, 3.18.1.1, and hardware versions: 0.102.19.1.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## GTR Lite {{ device("amazfit_gtr_lite") }}

## GTR Mini {{ device("amazfit_gtr_mini") }}

Tested with firmware versions: 5.2.18.1.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## GTS {{ device("amazfit_gts") }}

## GTS 2/2e {{ device("amazfit_gts_2") }}

## GTS 2 Mini {{ device("amazfit_gts_2_mini") }}

## GTS 3 {{ device("amazfit_gts_3") }}

Tested with firmware versions: 7.42.5.1, and hardware versions: 0.76.17.4.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## GTS 4 {{ device("amazfit_gts_4") }}

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## GTS 4 Mini {{ device("amazfit_gts_4_mini") }}

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Neo {{ device("amazfit_neo") }}

The Amazfit Neo has a simple monochrome LCD and an advertised battery life of ~28 days, which makes it much more like a regular watch than a smartwatch. Nonetheless it has sleep tracking and a heart rate sensor. Unlike some other Amazfit watches it uses a beeper instead of a vibrator.

Most features were added in {{ 2117|pull }} but a few features are not supported yet.

Supported features by Gadgetbridge:

* Notifications
* Find phone / watch
* Set alarm
* Live heart rate
* Set time format
* Most "display items" are displayed
* Download/sync activity data
* Setting automatic heart rate measurement interval
* Synchronizing time
* Weather
* Enabling/disabling beeps
* Inactivity notification
* Workout screen (feedback needed)
* PAI (feedback needed)

Does not work:

* Disconnect notification

## Pop {{ device("amazfit_pop") }}

## Pop Pro {{ device("amazfit_pop_pro") }}

## T-Rex {{ device("amazfit_trex") }}

## T-Rex 2 {{ device("amazfit_trex_2") }}

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## T-Rex Pro {{ device("amazfit_trex_pro") }}

## T-Rex Ultra {{ device("amazfit_trex_ultra") }}

Support for this gadget was added with {{ 3258|pull }}.

!!! tip "Zepp OS device"
    See the [Zepp OS page](../../basics/topics/zeppos.md) for a list of features and issues common to Zepp OS gadgets.

## Verge Lite {{ device("amazfit_verge_lite") }}

## X {{ device("amazfit_x") }}

Essentially the same as [Amazfit Band 5](#band-5) but without the extra VO2 max sensor.
