---
title: Asteroid OS
---

# Asteroid OS {{ device("asteroid_os") }}

[Asteroid OS](https://asteroidos.org/){ :target="_blank" } is an open-source linux distribution for smartwatches. It actually supports multiple watches, with a varying level of features. Make sure to check the [official website](https://asteroidos.org/watches/){ :target="_blank" } for the list of watches and their support.
