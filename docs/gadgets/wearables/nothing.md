---
title: Nothing
---

# Nothing

--8<-- "blocks.md:non_released_gadget"

## CMF Watch Pro {{ device("nothing_cmf_watch_pro") }}

The CMF Watch Pro is highly supported by Gadgetbridge, with just a couple of known issues and missing features.

Tested on firmware 11.0.0.50, model D395.

!!! warning "Server pairing"
    An auth key obtained with root from the official app is required to pair this watch with Gadgetbridge - see the [Nothing CMF server pairing](../../basics/pairing/nothing-cmf-server.md) page for more information.

### Features

The following is an extensive list of all the features implemented in Gadgetbridge for the Nothing CMF Watch Pro:

* Authentication
* Battery
* Firmware version, serial number
* Set time and timezone
* Notifications, with icons
* Find watch from phone
* Find phone from watch
* Music info and buttons
* Activity sync
    * Steps
    * Calories, distance (no UI)
    * Heart Rate
    * Stress
    * SpO2
    * Sleep stages
    * Workouts and GPS tracks
* Settings
    * Goals (steps, calories, distance)
    * Lift wrist to wake screen
    * All-day monitoring (heart rate, SpO2, stress)
    * Heart alerts (low, high, high active, SpO2)
    * Time format (12h/24h)
    * Alarms
    * Contacts
    * Workouts available on watch
    * Inactivity reminder
    * Hydration reminder
    * Measurement system (length, temperature)
* Factory reset

### Known issues

* Alarms labels do not do anything on the watch, even in official app

### Missing features

The following feature are not yet implemented in Gadgetbridge:

* Fetch model number
* Firmware upgrade
* Do not disturb
* Set watchface from phone
* Weather
* Some settings
    * Call reminder
    * Language
* Read settings from watch
    * alarms
    * contacts
    * other settings
* Activity data
    * Resting heart rate

### Experimental features

The following features are implemented in Gadgetbridge, but experimental / not heavily tested.

#### Watchface upload

Watchface upload is implemented and somewhat working.

When installing a watchface on the official app, the file can be found under `/data/data/com.nothing.cmf.watch/com.nothing.cmf.watch/cache/dial/` for rooted devices.

#### AGPS upload

AGPS upload is implemented, but untested. The upload process seems to follow a very similar method as watchface upload.

It is currently not known how to obtain AGPS files.
