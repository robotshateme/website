---
title: Samsung
---

# Samsung

## Galaxy Buds (2019) {{ device("samsung_galaxy_buds_2019") }}

To add that gadget to Gadgetbridge, remove the buds from Android pairing and use Gadgetbridge to scan and then add it in. Allow it to pair. If you have issues pairing, make sure Wear app is either disabled or not installed.

Supported features by Gadgetbridge:

* Factory reset (in Debug options in Gadgetbridge)
* Battery readout and plotting for each of the buds. This version of earbuds does not report case battery values.
* Find buds function
* Ambient mode + tuning
* Equalizer + tuning
* Touch lock
* Touch events - long press: Voice Assistant, Volume, Quick ambient and Ambient sound.
* Game mode (no idea if this works, it does not seem to do anything)

## Galaxy Buds Live {{ device("samsung_galaxy_buds_live") }}

To add that gadget to Gadgetbridge, remove the buds from Android pairing and use Gadgetbridge to scan and then add it in. Allow it to pair. If you have issues pairing, make sure Wear app is either disabled or not installed.

Supported features by Gadgetbridge:

* Factory reset (in Debug options in Gadgetbridge)
* Battery readout and plotting for each of the buds and for the case.
* Find buds function
* Active Noise Canceling
* Equalizer
* Touch lock
* Touch events - long press: Voice Assistant, Volume, Active Noise Canceling
* Game mode (no idea if this works, it does not seem to do anything)
* Pressure relief with Ambient Sound

## Galaxy Buds Pro {{ device("samsung_galaxy_buds_pro") }}

To add that gadget to Gadgetbridge, remove the buds from Android pairing and use Gadgetbridge to scan and then add it in. Allow it to pair. If you have issues pairing, make sure Wear app is either disabled or not installed.

Supported features by Gadgetbridge:

* Factory reset (in Debug options in Gadgetbridge)
* Battery readout and plotting for each of the buds and for the case.
* Find buds function
* Active Noise Canceling
* Ambient Sound
* Equalizer
* Touch events - Voice Assistant, Volume, Active Noise Canceling, Lock, Double tab edge
* Settings
    * Ear detection
    * Seamless switch
    * Noise control with one bud
    * Balance
    * Game mode
* Ambient sound
    * Voice detect
    * During call
    * Volume L/R
    * Tone

## Galaxy Buds 2 {{ device("samsung_galaxy_buds_2") }}

Support was added in {{ 2853 | pull }}.

Working:

- Pairing
- Earbud and case battery level
- Finding lost device
- Settings:
	- Noise control:
    	- ANC/ambient/off
        - With one earbud
	- Ambient sound:
		- Customization
        - Volume
    - Touch options:
    	- Touch lock
        - Switch noise control, voice assistant, Spotify and volume actions
    	- Double tap edge
    - Equalizer
    - Sound Balance
    - Ambient sound during calls
    - In-ear detection

Untested settings:

* Game mode

## Galaxy Buds 2 Pro {{ device("samsung_galaxy_buds_2_pro") }}

Support was added in {{ 3049 | pull }}.

Working:

* Pairing
* Earbud and case battery level
* Finding lost device
* Settings:
    * Noise control:
        * ANC/ambient/off
        * With one earbud
        * Voice detect and timeouts
        * Ambient sound during calls
    * Touch options:
        * Touch lock
        * Switch noise control, voice assistant, Spotify and volume actions
        * Double tap edge
    * Equalizer
    * Sound balance
    * Seamless earbud connection

Untested settings:

* In-ear detection for calls
* Ambient sound customization
* Game mode
