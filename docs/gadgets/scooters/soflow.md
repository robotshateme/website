---
title: SoFlow
---

# SoFlow

## SO6 {{ device("soflow_so6") }}

Support for this gadget was added with {{ "27406e0881"|commit }}.

The only thing Gadgetbridge with the Scooter is to lock and unlock it.
It seems that communication is encrypted with a key that is equal for all devices, at least when they have not been paired with the official app.

The key can be found in the apk of the official app and has to be set as auth key in Gadgetbridge.

In other words, you can use this to lock and unlock your scooter. Als well as every other SO6 that has not been paired yet with the offical app.