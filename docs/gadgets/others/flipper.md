---
title: Flipper
---

# Flipper

## Zero {{ device("flipper_zero") }}

> Flipper Zero is a portable multi-tool for pentesters and geeks in a toy-like body. It loves hacking digital stuff, such as radio protocols, access control systems, hardware and more. It's fully open-source and customizable, so you can extend it in whatever way you like.

Source: [Flipper Zero's website](https://flipperzero.one/){: target="_blank" }

It's main purpose currently is to provide an Intent-based API to Tasker and similar apps to play sub-GHz files. In the future, file management and other features might be useful.

See [Gadget-specific intents](../../internals/automations/intents-gadget.md#flipper-zero) for more details.