---
title: Xiaomi
---

# Xiaomi

## Mijia LYWSD02 {{ device("mijia_lywsd02") }}

A smart clock with temperature and humidity monitoring.

## Mijia LYWSD02MMC {{ device("mijia_lywsd02mmc") }}

Given details under [Mijia LYWSD02](#mijia-lywsd02) also apply to this gadget.

Support for this gadget was added with {{ 3316|pull }}.

## Mijia LYWSD03MMC {{ device("mijia_lywsd03mmc") }}

Also known as the Mijia Temperature and Humidity Sensor 2.

A simple temperature and humidity sensor. Same protocol as the LYWSD02, but does not have a clock / does not support setting the time.

Tested with HW B2.0 and FW 1.0.0_0130.

## Mijia MHO-C303 {{ device("mijia_mho_c303") }}

Similar to the LYWSD02, but it runs on 2 AAA batteries.

Added based on feedback from {{ 3513 | issue }}.
