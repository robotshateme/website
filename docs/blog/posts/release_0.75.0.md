---
slug: release-0_75_0
date: 2023-07-05T00:00:00
authors:
  - ashimokawa
categories:
  - Releases
---

# Gadgetbridge 0.75.0: Native OsmAnd support

Again after more than 1.5 month, a new Gadgetbridge release finally is ready!
The F-Droid black box is still black though. :P

Traditionally we bump the release by a non-point release when support for a new
device gets added or a big new code feature gets added, this time the latter is
the case! We finally merged native OsmAnd support, a branch was started by
me nearly two years ago but only recently picked up and fixed by José.
PineTime/InfiniTime and bangle.js are the first watches that add support for
making use of navigation instructions passed to devices by Gadgetbridge.
There could be potential support for Pebble and Fossil/Skagen HR via watchapps
in the future.
<!-- more -->

Of course, Gadgetbridge 0.75 also includes the usual fixes and improvements.

On the Bangje.js front, Gordon and contributors contributed a lot of small but
important fixes (see Changelog), for the Fossil/Skagen HR, Arjan mainly improved
weather support to potentially show UV index and chance of rain (given weather
apps provide this info to Gadgetbridge.

ZeppOS support, mainly maintained by José also saw a lot of improvements. Much
more than we can write about, again, please see the Changelog below, also to
know about other generic changes and improvements not mentioned here.

Thank you for your continuous support, and we hope you will like Gadgetbridge
0.75!

This blog post was written by a human :)

Andreas

### Changelog

#### 0.75.0
* Bangle.js: Add message size limitation to Calendar and Messages
* Bangle.js: Add switch to control if the GPS chip should be used to locate the location
* Bangle.js: Send more weather data to watch
* Bangle.js: Allow an activity sample to have a timestamp
* Bangle.js: Send last received activity timestamp on connect (to allow sync of activity samples)
* Bangle.js: Allow connecting HW keyboard without closing app loader
* Bangle.js: Bump flavor target SDK version to 31
* Bangle.js: Fix convertion of emoji/unicode to bitmap without width/height
* Bangle.js: Fix location listener not being cleaned up when waiting for reconnect
* Bangle.js: Fix memory leak from HTTP requests
* Bangle.js: Fix orientation changes closing app loader
* Bangle.js: Fix return to applications management activity after having opened another window
* Bangle.js: Set default value for GPS event interval to 1 second
* Bangle.js: Support navigation instructions
* Bangle.js: Escape characters that fall in the Unicode codepoint area (for Espruino ~2v18.20 and later)
* Bangle.js: HTTP request XPath can now return Arrays
* Fossil/Skagen Hybrids: Add support for ultraviolet index and rain probability
* Fossil/Skagen Hybrids: Add UV index and chance of rain widgets
* Fossil/Skagen Hybrids: Allow launching the calibration activity on any Gadgetbridge variant
* Fossil/Skagen Hybrids: Increase accuracy of workout distance calculation
* Fossil/Skagen Hybrids: Fix weather icons day/night status
* InfiniTime: Fix weather expiry time
* InfiniTime: Support navigation instructions
* Mi Band 6: Allow making device discoverable via Bluetooth when connected
* Mi Band 7: Add preference to display call contact information
* Zepp OS: Add gpx route file upload
* Zepp OS: Add screenshot support
* Zepp OS: Add stress charts
* Zepp OS: Add watch app logs developer option
* Zepp OS: Display watchface and app preview on install
* Zepp OS: Fix update operations on Zepp OS 2.1+
* Zepp OS: Manage contacts on watch
* Zepp OS: Start new GPX segments on pause/resume
* Zepp OS: Support flashing zab files
* App Manager: Fix cached apps sorting
* App Manager: Hide drag handle if app reorder is not supported
* App Manager: Add confirmation before deleting app
* Add menus to share GPX, raw summary, raw details
* Debug Activity: Allow pairing current device as companion
* Fix some null pointer exception crashes
* Intent API: Add command to set device mac address
* Intent API: Add dataTypes parameter for activity sync
* Intent API: Add debug actions for notifications and incoming calls
* OsmAnd: Add support for navigation instructions
* Scrape navigation instructions from Google Maps notifications
* Fix lag when a folder has a lot of devices
* Fix transliteration returning non-ASCII characters
* Enable "allow high MTU" setting by default
* Make some hardcoded english strings translatable
