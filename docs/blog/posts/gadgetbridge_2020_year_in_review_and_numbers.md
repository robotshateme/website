---
slug: gadgetbridge_2020_year_in_review_and_numbers
date: 2020-12-31T00:00:00
authors:
  - vanous
categories:
  - About
---

# Gadgetbridge 2020: year in review and numbers

This has been a very busy but good year for Gadgetbridge, with 21 releases
since January 2020. It has been the first year after we have migrated to
[Codeberg](https://codeberg.org). One of the possible concern of the move has
been smaller visibility and loss of contributors, but opposite turned out to be
true. The visible favorite "star" data is of course lower - the previously
reached star numbers on the GitHub hosting went into several thousands (3600
stars), with 200 watchers and 627 forks and current data on Codeberg shows
smaller numbers after the first year: [173
stars](https://codeberg.org/Freeyourgadget/Gadgetbridge/stars), [22
watchers](https://codeberg.org/Freeyourgadget/Gadgetbridge/watchers) and [86
forks](https://codeberg.org/Freeyourgadget/Gadgetbridge/forks), but we did have
larger engagement and number of contributors. Big thank you to everybody being
involved with Gadgetbridge now or any time before!
<!-- more -->

## Commits, contributors and changed files

Here is a compiled table for numbers for all the years of the project:

<table class="table table-striped">
<thead>
<tr>
<th>Year</th>
<th>Files changed</th>
<th>Commits</th>
<th>Contributors</th>
</tr>
</thead>
<tbody>
<tr>
<td>2020</td>
<td>1304</td>
<td>1395</td>
<td>121</td>
</tr>
<tr>
<td>2019</td>
<td>805</td>
<td>933</td>
<td>107</td>
</tr>
<tr>
<td>2018</td>
<td>615</td>
<td>848</td>
<td>120</td>
</tr>
<tr>
<td>2017</td>
<td>597</td>
<td>1005</td>
<td>67</td>
</tr>
<tr>
<td>2016</td>
<td>421</td>
<td>1138</td>
<td>30</td>
</tr>
<tr>
<td>2015</td>
<td>303</td>
<td>970</td>
<td>14</td>
</tr>
</tbody>
</table>

<details>
<summary>Table was generated with this script (click to see).</summary>

```bash

    #!/bin/bash

    COMMITS="2020 71ab859c921a3352dc2ad17db2ddb187539510ab
    2019 cf870bff8a94b006b4a12b075ba24a1015b865bd
    2018 d570ee864d94d74a8b734a93f1abce3995a71569
    2017 1722a6dc47aeb66fe7f1bb10c5cde931279eabb2
    2016 50cd5b26294c38578077b6ad7e16b88b643711c1
    2015 8dd67b146d09801ae2be20ae818daa92995ec6ad"
    end="f9e6624646"


    #files changed: git diff 20cf15d91f...master --shortstat | awk -F " " '{print $1}'
    #commits: git rev-list 20cf15d91f...master | wc -l

    echo "|Year|Files changed|Commits|Contributors|"
    echo "|--|--|--|--|"
    IFS=$'\n'
    for LINE in $COMMITS
    do
        year=`echo $LINE | awk '{print $1}'`
        start=`echo $LINE | awk '{print $2}'`
        files=`git diff $start...$end --shortstat | awk -F " " '{print $1}'`
        commits=`git rev-list $start...$end | wc -l`
        commiters=`git log $start...$end --pretty=oneline --format='%aN' | sort | uniq | wc -l`
        end=$start
        echo "| $year | $files | $commits | $commiters |"
    done

```

</details>

It takes some time to understand Gadgetbridge codebase but we are happy to see
so many new people being able to do so. In order to make the process easier,
documentation has gradually been improved and the [new device
tutorial](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/New-Device-Tutorial)
has over time become a good guide for anyone with some coding skills, willing
to add new device support into Gadgetbridge.


## Pull requests

As for merge/pull requests it is a bit harder to compare, Codeberg [shows
us](https://codeberg.org/Freeyourgadget/Gadgetbridge/activity/yearly) 149
merged pull requests this year. The data of contributors and commits between
what the Gitea web interface shows for PRs and what git tells us are a bit
different, but we go with what we have.


## Issues

We have 515 currently opened issues in total and many of them stalled, but they
contain interesting suggestions, ideas, research, device requests and more, and
although "closed issue is still there", somehow we prefer not to close the
stalled ones. About 294 of these issues are active. We have closed 162 issues
with 132 new issues open in 2020, which is at least a positive trend, leading
to a smaller number of open issues.

## Contributors and active users

Contributors and active users are the main asset of any project and we are
happy to see new people stepping up and contributing with code, which is the
most lacking resource we have, as it is a bit easier to help with translations,
documentation or bug reports. 132 people have contributed this year and these
are people who made a contribution visible via git (code, translation and
wiki), but there are also people helping with reporting bugs, managing issues
and so on and we are thankful for all their help.

The Matrix chatroom available via
[Element](https://app.element.io/#/room/#gadgetbridge:matrix.org) has also been
a nice addition, allowing questions, chats and interaction outside of the issue
tracker, currently hosting 220 people. Some people do not prefer the
"encryption on" by default, causing some limited visibility of past
conversations, but given the
[history](https://blog.freeyourgadget.org/category/dmca.md), we are happy
with this approach.

One of our biggest concerns has been the gradual decline of contributions from
some members of the core team, which has been a big loss. It seems, however,
that over time we have been able to gain few permanent helpers and also some
drive-by contributors. Hopefully some of them will stick with us for longer
time :)

Documentation in [wiki](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki)
has seen many improvements and is a good entry way for any new potential
contributors with their first commits.

To make sure we acknowledge all 132 contributors (including wiki editors) of
the year 2020, here is a complete list, alphabetically sorted:

115ek, Abdullah Manaz, Adolfo Jayme Barrientos, AiLab, Ainārs, Alex, Allan
Nordhøy, Andreas Böhler, Andreas Shimokawa, Andrzej Surowiec, Andy Yang,
Anonymous, Artem, Asbesbopispa, Baka Gaijin, Bot, C0rn3j, CE4, Cristian Alfano,
Daniel Dakhno, Deactivated Account, Deixondit, Deril, Dmitriy Bogdanov, Dmitry
Markin, Dmytro Bielik, Fabio Parri, Florian Beuscher, FransM, Full Name, Gleb
Chekushin, Gordon Williams, Hanhan Husna, HenRy, HugoGEORGET, Igor Polyakov,
Izzy, J. Lavoie, JF, Jeannette L, Julian Lam, Kintu, KopfKrieg, License Bot,
LizardWithHat, Louis-Marie Croisez, Luis zas, Maciej Kuśnierz, Mamut, Marco
Alberto Diosdado Nava, Mario, Mario Rossi, Mattherix, Maxim Baz, Memiks,
Michael, Michal L, Milo Ivir, Mirko Covizzi, Nathan, Nee Sorry, NicoBuntu,
Nikita Epifanov, Nikolai Sinyov, Nur Aiman Fadel, Ondřej Sedláček, Oğuz Ersen,
Pander, Pauli Salmenrinne, Pavel, Rafael Fontenelle, Rajesh Kumbhakar, Retew,
Roxystar, Samuel Carvalho de Araújo, Saul Nunez, Sebastian Espinosa, Sebastian
Obrusiewicz, SnowCat, Szylu, Taavi E, TaaviE, Ted Stein, TinfoilSubmarine, Toby
Murray, Unixware, Vincèn PUJOL, Vladislav Glinsky, Vytenis, Yaron Shahrabani,
Yukai Li, Zhong Jianxin, andyboeh, angelpup, anonaii, anonymous, bride_salon,
ce4, chabotsi, cpfeiffer, dakhnod, fparri, frederic lesur LESUR　Frederic,
hackoder, homocomputeris, ildar, izzy, ksiwczynski, luca sain, lucanomax,
m-p{3}, mamutcho, marclaporte, marco.altomonte, mkusnierz, mondstern,
nautilusx, odavo32nof, opavlov, pangwalla, postsorino, rababerladuseladim,
rffontenelle, ssantos, t-m-w, taras3333, vanous, vishnu, xaos, zsolt3991,
Étienne Deparis, 陈少举 

<details>
<summary>List was generated with this command (click to see).</summary>

```
#in Gb repo:
git log 71ab859c921a3352dc2ad17db2ddb187539510ab...master --pretty=oneline --format='%aN' | sort | uniq
#in wiki repo:
git log b735016...master --pretty=oneline --format='%aN' | sort | uniq
```
</details>


## Past and the Future

Work in 2020 has brought support for 15 new devices: JYou Y5, iTag, Fossil
Hybrid HR, WatchX(Plus), TLW64, Mi Band 5, Pinetime-JF Infinitime, Sony SWR12
bands, Lefun Smart Bands, Nut, Amazfit Bip S Lite, Amazfit GTR 2/GTS 2, Casio
GBX-100, Amazfit Bip U and Amazfit Verge Lite. We are happy to see some open
source, open hardware watches finally entering the market and gradually
becoming more useful, starting to compete with commercial offerings. Several
parts of Gadgetbridge have been reworked including Bluetooth pairing, Sports
Activities parsing and displaying, Daily activities, user settings, we gained
some more automation possibilities of reacting to button and device events,
cleaned up and converted to vector format many icons, fixed many bugs... as per
our regular [blog posts](https://blog.freeyourgadget.org/).

As for the future and what are the plans for Gadgetbridge are, it would be best
to hear from our users and even more from people who can contribute changes
they would like to see implemented in Gadgetbridge. Also, there are several
tasks that need to be tackled related to Android 10/11 changes in permissions
so we would be happy to accept contribution in this direction.

We thank you for all your support and hope for another good year while keeping
the main promises of Gadgetbridge: Your data about you stays yours. Your
conversations forwarded as notifications aren't going anywhere else.
