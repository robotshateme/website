#
#   device_support.yml
#
#   This file is used for adding metadata/flags to gadgets 
#   which is shown in pages under "docs/gadgets" directory.
#
#   Then, the status of gadget can be obtained in markdown
#   via "{{ device("xiaomi_mi_band_1") }}" tag.
#
#   NOTE: Make sure to add {{ device("xiaomi_mi_band_1") }} 
#         after the header name, for example:
#
#   ## Mi Band 1 {{ device("xiaomi_mi_band_1") }}
#
#   Since the device() function also inserts a anchor ID for header, 
#   it needs to be placed next to the header name, so the header will
#   now have "xiaomi_mi_band_1" as anchor, regardless of its header
#   text.
#
#   Every gadget which is supported by Gadgetbridge MUST be
#   included in this page, but this rule is not enforced yet.
#
#   This file also can be used for building APIs and feeds 
#   (like RSS), so users can be notified for when their 
#   gadget is now supported by Gadgetbridge.
#
__aliases:
  - &flags_unknown [feature_unknown, pair_unknown]
  - &flags_unknown_free [feature_unknown, pair_free]
  - &flags_high_free [feature_high, pair_free]
  - &flags_high_huami [feature_high, pair_huami]
  - &flags_unknown_huami [feature_unknown, pair_huami]
  - &flags_high_fossil [feature_high, pair_fossil]
  - &flags_zepp_os [feature_high, pair_huami, os_zepp]
  - &flags_zepp_os_untested [feature_most, pair_huami, experimental, os_zepp]
  - &flags_zepp_os_3 [feature_partial, pair_huami, os_zepp]
  - &link_mi_band_1_firmware ../../../internals/specifics/mi-band-1
  - &link_mi_band_2_firmware ../../../internals/specifics/mi-band-2
  - &link_mi_band_3_firmware ../../../internals/specifics/mi-band-3
  - &link_mi_band_4_firmware ../../../internals/specifics/mi-band-4
  - &link_mi_band_5_firmware ../../../internals/specifics/mi-band-5
  - &link_mi_band_6_firmware ../../../internals/specifics/mi-band-6
  - &link_amazfit_band_5_firmware ../../../internals/specifics/amazfit-band-5
  - &link_amazfit_bip_firmware ../../../internals/specifics/amazfit-bip
  - &link_amazfit_bip_os ../../../internals/specifics/amazfit-bip-os
  - &link_amazfit_bip_s_firmware ../../../internals/specifics/amazfit-bip-s
  - &link_amazfit_bip_u_firmware ../../../internals/specifics/amazfit-bip-u
  - &link_amazfit_cor_firmware ../../../internals/specifics/amazfit-cor
  - &link_amazfit_cor_2_firmware ../../../internals/specifics/amazfit-cor-2
  - &link_fossil_firmware ../../../internals/specifics/fossil-hybrid
  - &link_pebble_firmware ../../../internals/specifics/pebble
  - &link_pebble_language ../../../internals/specifics/pebble-language
  - &link_pebble_datalog ../../../internals/specifics/pebble-datalog
  - &link_casio_protocol ../../../internals/specifics/casio-protocol
  - &link_banglejs_protocol ../../../internals/specifics/banglejs-protocol
  - &link_galaxy_buds_protocol ../../../internals/specifics/galaxy-buds-protocol
  - &link_fitpro_protocol ../../../internals/specifics/fitpro-protocol
  - &link_hplus_protocol ../../../internals/specifics/hplus-protocol
  - &link_garmin_protocol ../../../internals/specifics/garmin-protocol
  - &link_supercars_protocol ../../../internals/specifics/supercars-protocol
  - &link_zetime_protocol ../../../internals/specifics/zetime-protocol
  - &flags_huawei [feature_most, pair_free]
device_support:
  xiaomi_mi_band_1:
    type: wearable
    vendor: xiaomi
    flags: *flags_high_free
    links:
      firmware_info: *link_mi_band_1_firmware
  xiaomi_mi_band_1a:
    type: wearable
    vendor: xiaomi
    flags: *flags_high_free
    links:
      firmware_info: *link_mi_band_1_firmware
  xiaomi_mi_band_1s:
    type: wearable
    vendor: xiaomi
    flags: *flags_high_free
    links:
      firmware_info: *link_mi_band_1_firmware
  xiaomi_mi_band_2:
    type: wearable
    vendor: xiaomi
    flags: *flags_high_free
    links:
      firmware_info: *link_mi_band_2_firmware
  xiaomi_mi_band_3:
    type: wearable
    vendor: xiaomi
    flags: *flags_high_free
    links:
      firmware_info: *link_mi_band_3_firmware
  xiaomi_mi_band_4:
    type: wearable
    vendor: xiaomi
    flags: *flags_high_huami
    links:
      firmware_info: *link_mi_band_4_firmware
  xiaomi_mi_band_5:
    type: wearable
    vendor: xiaomi
    flags: *flags_high_huami
    links:
      firmware_info: *link_mi_band_5_firmware
  xiaomi_mi_band_6:
    type: wearable
    vendor: xiaomi
    flags: *flags_high_huami
    links:
      firmware_info: *link_mi_band_6_firmware
  xiaomi_mi_band_7:
    vendor: xiaomi
    type: wearable
    flags: *flags_zepp_os
  xiaomi_mi_band_8:
    vendor: xiaomi
    type: wearable
    flags: [feature_high, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_mi_band_8_pro:
    vendor: xiaomi
    type: wearable
    flags: [feature_unknown, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_mi_band_7_pro:
    vendor: xiaomi
    type: wearable
    flags: [feature_most, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_mi_watch_color_sport:
    vendor: xiaomi
    type: wearable
    flags: [feature_most, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_mi_watch_lite:
    vendor: xiaomi
    type: wearable
    flags: [feature_most, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_redmi_smart_band_2:
    vendor: xiaomi
    type: wearable
    flags: [feature_most, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_redmi_smart_band_pro:
    vendor: xiaomi
    type: wearable
    flags: [feature_most, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_redmi_watch_2:
    vendor: xiaomi
    type: wearable
    flags: [feature_unknown, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_redmi_watch_2_lite:
    vendor: xiaomi
    type: wearable
    flags: [feature_unknown, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_redmi_watch_3:
    vendor: xiaomi
    type: wearable
    flags: [feature_unknown, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_redmi_watch_3_active:
    vendor: xiaomi
    type: wearable
    flags: [feature_unknown, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_watch_s1_active:
    vendor: xiaomi
    type: wearable
    flags: [feature_unknown, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_watch_s1:
    vendor: xiaomi
    type: wearable
    flags: [feature_unknown, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_watch_s1_pro:
    vendor: xiaomi
    type: wearable
    flags: [feature_most, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_watch_s3:
    vendor: xiaomi
    type: wearable
    flags: [feature_poor, experimental, os_xiaomi_protobuf, pair_xiaomi]
  xiaomi_scale_2:
    vendor: xiaomi
    type: scale
    flags: [feature_poor, pair_unknown]
  roidmi_fm:
    vendor: xiaomi
    type: fm_transmitter
    flags: *flags_high_free
  roidmi_fm_2:
    vendor: xiaomi
    type: fm_transmitter
    flags: *flags_high_free
  roidmi_fm_3:
    vendor: xiaomi
    type: fm_transmitter
    flags: *flags_high_free
  mijia_lywsd02:
    vendor: xiaomi
    type: clock
    flags: [feature_partial, pair_free]
  mijia_lywsd02mmc:
    vendor: xiaomi
    type: clock
    flags: [feature_partial, pair_free]
  mijia_lywsd03mmc:
    vendor: xiaomi
    type: clock
    flags: [feature_partial, pair_free]
  mijia_mho_c303:
    vendor: xiaomi
    type: clock
    flags: [feature_partial, pair_free]
  #
  #   Amazfit
  #
  amazfit_band_5:
    type: wearable
    vendor: amazfit
    flags: *flags_high_huami
    links:
      firmware_info: *link_amazfit_band_5_firmware
  amazfit_band_7:
    type: wearable
    vendor: amazfit
    flags: *flags_zepp_os
  amazfit_bip:
    type: wearable
    vendor: amazfit
    flags: *flags_high_free
    links:
      firmware_info: *link_amazfit_bip_firmware
      custom_os: *link_amazfit_bip_os
  amazfit_bip_lite:
    type: wearable
    vendor: amazfit
    flags: *flags_high_huami
  amazfit_bip_s_lite:
    type: wearable
    vendor: amazfit
    flags: *flags_unknown_huami
  amazfit_bip_s:
    type: wearable
    vendor: amazfit
    flags: *flags_high_huami
    links:
      firmware_info: *link_amazfit_bip_s_firmware
  amazfit_bip_u:
    type: wearable
    vendor: amazfit
    flags: *flags_high_huami
    links:
      firmware_info: *link_amazfit_bip_u_firmware
  amazfit_bip_u_pro:
    type: wearable
    vendor: amazfit
    flags: *flags_high_huami
    links:
      firmware_info: *link_amazfit_bip_u_firmware
  amazfit_bip_3_pro:
    type: wearable
    vendor: amazfit
    flags: *flags_high_huami
  amazfit_bip_5:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_cor:
    type: wearable
    vendor: amazfit
    flags: *flags_high_huami
    links:
      firmware_info: *link_amazfit_cor_firmware
  amazfit_cor_2:
    vendor: amazfit
    type: wearable
    flags: *flags_high_huami
    links:
      firmware_info: *link_amazfit_cor_2_firmware
  amazfit_gtr:
    vendor: amazfit
    type: wearable
    flags: *flags_unknown
  amazfit_gtr_2:
    vendor: amazfit
    type: wearable
    flags: *flags_unknown
  amazfit_gtr_3:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_gtr_3_pro:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_gtr_4:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_gtr_lite:
    type: wearable
    vendor: amazfit
    flags: *flags_unknown_huami
  amazfit_gtr_mini:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os_untested
  amazfit_gts:
    vendor: amazfit
    type: wearable
    flags: *flags_unknown
  amazfit_gts_2:
    vendor: amazfit
    type: wearable
    flags: *flags_unknown
  amazfit_gts_2_mini:
    type: wearable
    vendor: amazfit
    flags: *flags_unknown_huami
  amazfit_gts_3:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_gts_4:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_gts_4_mini:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_neo:
    type: wearable
    flags: *flags_high_huami
    vendor: amazfit
  amazfit_pop:
    type: wearable
    vendor: amazfit
    flags: *flags_unknown
  amazfit_pop_pro:
    type: wearable
    vendor: amazfit
    flags: *flags_unknown
  amazfit_falcon:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_trex:
    vendor: amazfit
    type: wearable
    flags: *flags_unknown
  amazfit_trex_pro:
    vendor: amazfit
    type: wearable
    flags: *flags_unknown
  amazfit_trex_2:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_trex_ultra:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_cheetah:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os_untested
  amazfit_cheetah_pro:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_verge_lite:
    vendor: amazfit
    type: wearable
    flags: [feature_unknown, pair_huami]
  amazfit_x:
    type: wearable
    flags: *flags_high_huami
    vendor: amazfit
  amazfit_balance:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os_3
  amazfit_active:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os
  amazfit_active_edge:
    vendor: amazfit
    type: wearable
    flags: *flags_zepp_os_untested
  bangle_js_1:
    type: wearable
    vendor: bangle.js
    flags: *flags_high_free
    links:
      protocol: *link_banglejs_protocol
  bangle_js_2:
    type: wearable
    vendor: bangle.js
    flags: *flags_high_free
    links:
      protocol: *link_banglejs_protocol
  asteroid_os:
    type: wearable
    vendor: other
    flags: *flags_unknown_free
  wasp_os:
    type: wearable
    vendor: other
    flags: *flags_unknown_free
  divoom_pixoo:
    type: other
    vendor: divoom
    flags: [feature_poor, pair_free]
  #
  #   Huawei / Honor
  #
  honor_band_3:
    vendor: honor
    type: wearable
    flags: *flags_huawei
  honor_band_4:
    vendor: honor
    type: wearable
    flags: *flags_huawei
  honor_band_5:
    vendor: honor
    type: wearable
    flags: *flags_huawei
  honor_band_6:
    vendor: honor
    type: wearable
    flags: *flags_huawei
  honor_band_7:
    vendor: honor
    type: wearable
    flags: [feature_unknown, pair_free, experimental]
  honor_magicwatch_2:
    vendor: honor
    type: wearable
    flags: *flags_huawei
  huawei_band_4:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  huawei_band_4_pro:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  huawei_band_6:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  huawei_band_7:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  huawei_band_8:
    vendor: huawei
    type: wearable
    flags: [feature_poor, pair_free, experimental]
  huawei_band_3e:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  huawei_band_4e:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  huawei_talk_band_b6:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  huawei_watch_gt:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  huawei_watch_gt_2:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  huawei_watch_gt_2_pro:
    vendor: huawei
    type: wearable
    flags: [feature_poor, pair_free, experimental]
  huawei_watch_gt_2e:
    vendor: huawei
    type: wearable
    flags: [feature_poor, pair_free, experimental]
  huawei_watch_gt_3:
    vendor: huawei
    type: wearable
    flags: [feature_poor, pair_free, experimental]
  huawei_watch_gt_3_pro:
    vendor: huawei
    type: wearable
    flags: [feature_poor, pair_free, experimental]
  huawei_watch_fit:
    vendor: huawei
    type: wearable
    flags: *flags_huawei
  #
  #   Casio
  #
  casio_gb-5600b:
    vendor: casio
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_casio_protocol
  casio_gb-6900b:
    vendor: casio
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_casio_protocol
  casio_gb-x6900b:
    vendor: casio
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_casio_protocol
  casio_stb-1000:
    vendor: casio
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_casio_protocol
  casio_gbx-100:
    vendor: casio
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_casio_protocol
  casio_gbd-100:
    vendor: casio
    type: wearable
    flags: [experimental, pair_free]
    links:
      protocol: *link_casio_protocol
  casio_gbd-200:
    vendor: casio
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_casio_protocol
  casio_gbd-h1000:
    vendor: casio
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_casio_protocol
  casio_gw-b5600:
    vendor: casio
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_casio_protocol
  casio_gmw-b5000:
    vendor: casio
    type: wearable
    flags: [experimental, pair_free]
    links:
      protocol: *link_casio_protocol
  #
  #   FitPro
  #
  fitpro_m6:
    vendor: fitpro
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_fitpro_protocol
  fitpro_m4:
    vendor: fitpro
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_fitpro_protocol
  fitpro_lh716:
    vendor: fitpro
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_fitpro_protocol
  fitpro_sunset_6:
    vendor: fitpro
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_fitpro_protocol
  fitpro_watch_7:
    vendor: fitpro
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_fitpro_protocol
  fitpro_fit_1900:
    vendor: fitpro
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_fitpro_protocol
  colacao_2021:
    vendor: fitpro
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_fitpro_protocol
  colacao_2023:
    vendor: fitpro
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_fitpro_protocol
  #
  #   Fossil
  #
  fossil_hybrid_hr:
    vendor: fossil
    type: wearable
    flags: *flags_high_fossil
    links:
      firmware_info: *link_fossil_firmware
  fossil_gen_6_hybrid:
    vendor: fossil
    type: wearable
    flags: *flags_high_fossil
    links:
      firmware_info: *link_fossil_firmware
  skagen_jorn_gen_6:
    vendor: fossil
    type: wearable
    flags: *flags_high_fossil
    links:
      firmware_info: *link_fossil_firmware
  fossil_q_hybrid:
    vendor: fossil
    type: wearable
    flags: *flags_unknown
  #
  #   Garmin
  #
  garmin_vivomove_hr:
    vendor: garmin
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_garmin_protocol
  #
  #   HPlus
  #
  hplus_zeblaze_zeband:
    vendor: hplus
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_hplus_protocol
  hplus_zeblaze_zeband_plus:
    vendor: hplus
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_hplus_protocol
  hplus_makibes_f68:
    vendor: hplus
    type: wearable
    flags: [feature_most, pair_free]
    links:
      protocol: *link_hplus_protocol
  #
  #   Samsung
  #
  samsung_galaxy_buds_2019:
    vendor: samsung
    type: headphone
    flags: *flags_high_free
    links:
      protocol: *link_galaxy_buds_protocol
  samsung_galaxy_buds_live:
    vendor: samsung
    type: headphone
    flags: *flags_high_free
    links:
      protocol: *link_galaxy_buds_protocol
  samsung_galaxy_buds_pro:
    vendor: samsung
    type: headphone
    flags: *flags_high_free
    links:
      protocol: *link_galaxy_buds_protocol
  samsung_galaxy_buds_2:
    vendor: samsung
    type: headphone
    flags: *flags_high_free
    links:
      protocol: *link_galaxy_buds_protocol
  samsung_galaxy_buds_2_pro:
    vendor: samsung
    type: headphone
    flags: *flags_high_free
    links:
      protocol: *link_galaxy_buds_protocol
  #
  #   Nothing
  #
  nothing_ear_1:
    vendor: nothing
    type: headphone
    flags: *flags_high_free
  nothing_ear_2:
    vendor: nothing
    type: headphone
    flags: *flags_high_free
  nothing_ear_stick:
    vendor: nothing
    type: headphone
    flags: *flags_high_free
  nothing_cmf_watch_pro:
    vendor: nothing
    type: wearable
    flags: [feature_high, pair_nothing_cmf]
  #
  #   Bose
  #
  bose_qc35:
    vendor: bose
    type: headphone
    flags: *flags_unknown_free
  #
  #   Sony
  #
  sony_wh-1000xm2:
    vendor: sony
    type: headphone
    flags: *flags_high_free
  sony_wh-1000xm3:
    vendor: sony
    type: headphone
    flags: *flags_high_free
  sony_wh-1000xm4:
    vendor: sony
    type: headphone
    flags: *flags_high_free
  sony_wf-sp800n:
    vendor: sony
    type: headphone
    flags: *flags_high_free
  sony_wf-1000xm3:
    vendor: sony
    type: headphone
    flags: *flags_high_free
  sony_wf-1000xm4:
    vendor: sony
    type: headphone
    flags: *flags_high_free
  sony_linkbuds_s:
    vendor: sony
    type: headphone
    flags: [feature_partial, pair_free]
  sony_wh-1000xm5:
    vendor: sony
    type: headphone
    flags: [feature_partial, pair_free]
  sony_wena_3:
    vendor: sony
    type: wearable
    flags: *flags_high_free
  sony_wf-1000xm5:
    vendor: sony
    type: headphone
    flags: [feature_most, pair_free, experimental]
  sony_wi-sp600n:
    vendor: sony
    type: headphone
    flags: *flags_high_free
  #
  #   iTag
  #
  itag_any:
    vendor: itag
    type: tracker
    flags: *flags_unknown_free
  #
  #   Lenovo
  #
  lenovo_watch_9:
    vendor: lenovo
    type: wearable
    flags: *flags_unknown_free
  lenovo_watch_x:
    vendor: lenovo
    type: wearable
    flags: *flags_high_free
  lenovo_watch_x_plus:
    vendor: lenovo
    type: wearable
    flags: *flags_high_free
  #
  #   Pebble
  #
  pebble_classic:
    type: wearable
    vendor: pebble
    flags: *flags_high_free
    links:
      firmware_info: *link_pebble_firmware
      language_pack: *link_pebble_language
      protocol: *link_pebble_datalog
  pebble_steel:
    type: wearable
    vendor: pebble
    flags: *flags_high_free
    links:
      firmware_info: *link_pebble_firmware
      language_pack: *link_pebble_language
      protocol: *link_pebble_datalog
  pebble_time:
    type: wearable
    vendor: pebble
    flags: *flags_high_free
    links:
      firmware_info: *link_pebble_firmware
      language_pack: *link_pebble_language
      protocol: *link_pebble_datalog
  pebble_time_steel:
    type: wearable
    vendor: pebble
    flags: *flags_high_free
    links:
      firmware_info: *link_pebble_firmware
      language_pack: *link_pebble_language
      protocol: *link_pebble_datalog
  pebble_time_round:
    type: wearable
    vendor: pebble
    flags: *flags_high_free
    links:
      firmware_info: *link_pebble_firmware
      language_pack: *link_pebble_language
      protocol: *link_pebble_datalog
  pebble_classic_2:
    type: wearable
    vendor: pebble
    flags: *flags_high_free
    links:
      firmware_info: *link_pebble_firmware
      language_pack: *link_pebble_language
      protocol: *link_pebble_datalog
  #
  #   SMA
  #
  sma_q2:
    vendor: sma
    type: wearable
    flags: [feature_poor, pair_free]
  sma_q3:
    vendor: sma
    type: wearable
    flags: *flags_unknown_free
  #
  #   Others
  #
  flipper_zero:
    vendor: flipper
    type: other
    flags: [feature_partial, pair_free]
  shell_racing_rc_motorsport:
    vendor: shell_racing
    type: other
    flags: *flags_high_free
    links:
      protocol: *link_supercars_protocol
  vibratissimo_any:
    vendor: vibratissimo
    type: other
    flags: *flags_high_free
  other_bfh-16:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  other_id115:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  other_id115_plus:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  other_bohemic_smart_bracelet:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  jyou_y5:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  sony_ericsson_liveview:
    vendor: sony
    type: wearable
    flags: *flags_unknown_free
  sony_swr12:
    vendor: sony
    type: wearable
    flags: *flags_unknown_free
  makibes_hr3:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  other_no_f1:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  pine64_pinetime:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  other_tlw64:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  other_teclast_h10:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  other_teclast_h30:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  soflow_so6:
    vendor: soflow
    type: scooter
    flags: [feature_poor, pair_unknown]
  mykronoz_zetime:
    vendor: mykronoz
    type: wearable
    flags: *flags_unknown_free
    links:
      protocol: *link_zetime_protocol
  other_xwatch:
    vendor: other
    type: wearable
    flags: *flags_unknown_free
  other_vesc:
    vendor: other
    type: other
    flags: *flags_unknown_free
  other_um25:
    vendor: other
    type: other
    flags: *flags_unknown_free
  femometer_vinca_2:
    vendor: femometer
    type: thermometer
    flags: [feature_most, pair_free]
  withings_steel_hr:
    vendor: withings
    type: wearable
    flags: *flags_unknown_free
